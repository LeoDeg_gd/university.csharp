﻿namespace LeoDeg.HoroscopeInformation
{
	partial class MainWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose ();
			}
			base.Dispose (disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
			this.lblTitle = new System.Windows.Forms.Label();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.toolStripBtnShowInfo = new System.Windows.Forms.ToolStripButton();
			this.btnShowInfo = new System.Windows.Forms.Button();
			this.dtpDateOfBirth = new System.Windows.Forms.DateTimePicker();
			this.toolStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// lblTitle
			// 
			this.lblTitle.AutoSize = true;
			this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblTitle.Location = new System.Drawing.Point(48, 50);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(97, 20);
			this.lblTitle.TabIndex = 0;
			this.lblTitle.Text = "Date of birth";
			// 
			// toolStrip1
			// 
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripBtnShowInfo});
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(309, 25);
			this.toolStrip1.TabIndex = 1;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// toolStripBtnShowInfo
			// 
			this.toolStripBtnShowInfo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.toolStripBtnShowInfo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBtnShowInfo.Image")));
			this.toolStripBtnShowInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripBtnShowInfo.Name = "toolStripBtnShowInfo";
			this.toolStripBtnShowInfo.Size = new System.Drawing.Size(64, 22);
			this.toolStripBtnShowInfo.Text = "Show Info";
			this.toolStripBtnShowInfo.Click += new System.EventHandler(this.ToolStripButton_ShowInfo_Click);
			// 
			// btnShowInfo
			// 
			this.btnShowInfo.Location = new System.Drawing.Point(52, 142);
			this.btnShowInfo.Name = "btnShowInfo";
			this.btnShowInfo.Size = new System.Drawing.Size(202, 64);
			this.btnShowInfo.TabIndex = 8;
			this.btnShowInfo.Text = "Show Info";
			this.btnShowInfo.UseVisualStyleBackColor = true;
			this.btnShowInfo.Click += new System.EventHandler(this.Button_ShowInfo_Click);
			// 
			// dtpDateOfBirth
			// 
			this.dtpDateOfBirth.Location = new System.Drawing.Point(52, 83);
			this.dtpDateOfBirth.Name = "dtpDateOfBirth";
			this.dtpDateOfBirth.Size = new System.Drawing.Size(202, 20);
			this.dtpDateOfBirth.TabIndex = 10;
			// 
			// MainWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(309, 236);
			this.Controls.Add(this.dtpDateOfBirth);
			this.Controls.Add(this.btnShowInfo);
			this.Controls.Add(this.toolStrip1);
			this.Controls.Add(this.lblTitle);
			this.Name = "MainWindow";
			this.Text = "Horoscope Information";
			this.Load += new System.EventHandler(this.MainWindow_Load);
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.Button btnShowInfo;
		private System.Windows.Forms.ToolStripButton toolStripBtnShowInfo;
		private System.Windows.Forms.DateTimePicker dtpDateOfBirth;
	}
}

