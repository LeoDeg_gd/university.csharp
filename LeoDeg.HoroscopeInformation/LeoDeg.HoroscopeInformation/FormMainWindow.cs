﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeoDeg.HoroscopeInformation
{
	public partial class MainWindow : Form
	{
		HoroscopeMonthInformation horoscopeMonthInformation;
		HoroscopeYearInformation horoscopeYearInformation;

		public MainWindow ()
		{
			InitializeComponent ();
		}

		private void MainWindow_Load (object sender, EventArgs e)
		{
			horoscopeMonthInformation = new HoroscopeMonthInformation ();
			horoscopeYearInformation = new HoroscopeYearInformation ();
		}

		private void Button_ShowInfo_Click (object sender, EventArgs e)
		{
			ShowInfo ();
		}

		private void ToolStripButton_ShowInfo_Click (object sender, EventArgs e)
		{
			ShowInfo ();
		}

		private void ShowInfo ()
		{
			AppendUserDateInformation ();
			ShowMonthHoroscopeInfo ();
		}

		private void AppendUserDateInformation ()
		{
			horoscopeMonthInformation.SetDay (dtpDateOfBirth.Value.Date.Day);
			horoscopeMonthInformation.SetMonth (dtpDateOfBirth.Value.Date.Month);
			horoscopeYearInformation.SetYear (dtpDateOfBirth.Value.Date.Year);
		}

		private void ShowMonthHoroscopeInfo ()
		{
			StringBuilder stringBuilder = new StringBuilder ();

			stringBuilder.Append ("Date of Birth:");
			stringBuilder.Append (string.Format ("\n{0}, {1}, {2}", horoscopeMonthInformation.GetDay (), horoscopeMonthInformation.GetMonth (), horoscopeYearInformation.GetYear ()));
			stringBuilder.Append (string.Format ("\nTotal lived days: \n{0}", DateTime.Now.Subtract (dtpDateOfBirth.Value.Date)));

			stringBuilder.Append ("\n\nZodiac ");
			stringBuilder.Append ("\nSign Name: ");
			stringBuilder.Append (horoscopeMonthInformation.GetName ());
			stringBuilder.Append ("\nSign Date: from ");
			stringBuilder.Append (horoscopeMonthInformation.GetDateFrom ());
			stringBuilder.Append (" to ");
			stringBuilder.Append (horoscopeMonthInformation.GetDateTo ());
			stringBuilder.Append ("\nSign Description: ");
			stringBuilder.Append (horoscopeMonthInformation.GetDescription ());

			stringBuilder.Append ("\n\nChinese Zodiac");
			stringBuilder.Append ("\nSign Name: ");
			stringBuilder.Append (horoscopeYearInformation.GetName ());
			stringBuilder.Append ("\nSign Description: ");
			stringBuilder.Append (horoscopeYearInformation.GetDescription ());

			MessageBox.Show (stringBuilder.ToString ());
		}
	}
}
