﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LeoDeg.HoroscopeInformation
{
	class HoroscopeYearInformationXMLReader
	{
		private XDocument xmlDocument;

		public HoroscopeYearInformationXMLReader (string documentUri)
		{
			xmlDocument = XDocument.Load (documentUri);
		}

		public string GetName (HoroscopeYearTypes type)
		{
			return GetElementValue ("Name", type);
		}

		public string GetDescription (HoroscopeYearTypes type)
		{
			return  GetElementValue ("Description", type);
		}

		public string GetElementValue (string elementName, HoroscopeYearTypes type)
		{
			return xmlDocument.Root.Elements ()
				.Where (x => (int)x.Attribute ("ID") == (int)type)
				.Select (x => x.Element (elementName).Value)
				.FirstOrDefault ()
				.Trim ();
		}
	}
}
