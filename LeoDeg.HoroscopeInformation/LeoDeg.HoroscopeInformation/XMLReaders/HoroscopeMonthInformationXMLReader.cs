﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace LeoDeg.HoroscopeInformation
{
	class HoroscopeMonthInformationXMLReader
	{
		private XDocument xmlDocument;

		public HoroscopeMonthInformationXMLReader (string DocumentUri)
		{
			xmlDocument = XDocument.Load (DocumentUri);
		}

		public string GetName (HoroscopeMonthTypes type)
		{
			return GetElementValue ("Name", type);
		}

		public string GetDateFrom (HoroscopeMonthTypes type)
		{
			return GetElementValue ("DateFrom", type);
		}

		public string GetDateTo (HoroscopeMonthTypes type)
		{
			return GetElementValue ("DateTo", type);
		}

		public string GetDescription (HoroscopeMonthTypes type)
		{
			return GetElementValue ("Description", type);
		}

		public string GetElementValue (string elementName, HoroscopeMonthTypes type)
		{
			return xmlDocument.Root.Elements ()
				.Where (x => (int)x.Attribute ("ID") == (int)type)
				.Select (x => x.Element (elementName).Value)
				.FirstOrDefault ()
				.Trim ();
		}
	}
}
