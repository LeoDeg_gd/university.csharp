﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeoDeg.HoroscopeInformation
{
	enum HoroscopeMonthTypes
	{
		Capricorn = 1,
		Aquarius,
		Pisces,
		Aries,
		Taurus,
		Gemini,
		Cancer,
		Leo,
		Virgo,
		Libra,
		Scorpio,
		Sagittarius,
	}

	enum HoroscopeYearTypes
	{
		Rat = 1,
		Ox,
		Tiger,
		Rabbit,
		Dragon,
		Snake,
		Horse,
		Goat,
		Monkey,
		Rooster,
		Dog,
		Pig
	}
}
