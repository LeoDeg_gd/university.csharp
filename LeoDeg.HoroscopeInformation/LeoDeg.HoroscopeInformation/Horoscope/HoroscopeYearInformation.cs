﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeoDeg.HoroscopeInformation
{
	class HoroscopeYearInformation
	{
		private int year;
		private HoroscopeYearTypes type;
		private HoroscopeYearInformationXMLReader informationReader;

		public HoroscopeYearInformation (int year = 2000)
		{
			this.year = year;
			informationReader = new HoroscopeYearInformationXMLReader ("HoroscopeYearInformation.xml");
		}

		public void SetYear (int year)
		{
			if (year <= 0)
				throw new ArgumentOutOfRangeException ("Year date must be greater than 0");

			this.year = year; 
			DetermineHoroscopeType ();
		}

		private void DetermineHoroscopeType ()
		{
			// Formula: ((year - 4) % 12) + 1
			type = (HoroscopeYearTypes)((this.year - 4) % 12) + 1;
		}

		public int GetYear ()
		{
			return year;
		}

		public string GetName ()
		{
			return informationReader.GetName (type);
		}

		public string GetDescription ()
		{
			return informationReader.GetDescription (type);
		}

		public HoroscopeYearTypes GetHoroscopeType ()
		{
			return type;
		}
	}
}
