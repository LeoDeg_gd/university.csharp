﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mail;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace LeoDeg.HoroscopeInformation
{
	class HoroscopeMonthInformation
	{
		private int day;
		private int month;
		private HoroscopeMonthTypes type;
		private HoroscopeMonthInformationXMLReader informationReader;

		public HoroscopeMonthInformation (int day = 1, int month = 1)
		{
			this.day = day;
			this.month = month;
			informationReader = new HoroscopeMonthInformationXMLReader ("HoroscopeMonthInformation.xml");
		}

		public HoroscopeMonthTypes GetHoroscopeType ()
		{
			return type;
		}

		public void SetDay (int day)
		{
			if (day <= 0 || day > 31)
				throw new ArgumentOutOfRangeException ("Day date must be between 1 and 31");

			this.day = day;
			DetermineHoroscopeType ();
		}

		public int GetDay ()
		{
			return day;
		}

		public void SetMonth (int month)
		{
			if (month <= 0 || month > 12)
				throw new ArgumentOutOfRangeException ("Month date must be between 1 and 12");

			this.month = month;
			DetermineHoroscopeType ();
		}

		public int GetMonth ()
		{
			return month;
		}

		public string GetName ()
		{
			return informationReader.GetName (type);
		}

		public string GetDateFrom ()
		{
			return informationReader.GetDateFrom (type);
		}

		public string GetDateTo ()
		{
			return informationReader.GetDateTo (type);
		}

		public string GetDescription ()
		{
			return informationReader.GetDescription (type);
		}

		private void DetermineHoroscopeType ()
		{
			// Capricorn - Козерог
			if ((this.day > 21 && this.month == 12) || (this.day <= 19 && this.month == 1))
				type = HoroscopeMonthTypes.Capricorn;

			// Aquarius - Водолей
			else if ((this.day > 19 && this.month == 1) || (this.day <= 19 && this.month == 2))
				type = HoroscopeMonthTypes.Aquarius;

			// Pisces - Рыбы
			else if ((this.day > 19 && this.month == 2) || (this.day <= 20 && this.month == 3))
				type = HoroscopeMonthTypes.Pisces;

			// Aries - Овен
			else if ((this.day > 20 && this.month == 3) || (this.day <= 20 && this.month == 4))
				type = HoroscopeMonthTypes.Aries;

			// Taurus - Бык
			else if ((this.day > 20 && this.month == 4) || (this.day <= 20 && this.month == 5))
				type = HoroscopeMonthTypes.Taurus;

			// Gemini - Близнецы
			else if ((this.day > 20 && this.month == 5) || (this.day <= 20 && this.month == 6))
				type = HoroscopeMonthTypes.Gemini;

			// Cancer - Рак
			else if ((this.day > 20 && this.month == 6) || (this.day <= 22 && this.month == 7))
				type = HoroscopeMonthTypes.Cancer;

			// Leo - Лев
			else if ((this.day > 22 && this.month == 7) || (this.day <= 22 && this.month == 8))
				type = HoroscopeMonthTypes.Leo;

			// Virgo - Дева
			else if ((this.day > 22 && this.month == 8) || (this.day <= 22 && this.month == 9))
				type = HoroscopeMonthTypes.Virgo;

			// Libra - Весы
			else if ((this.day > 22 && this.month == 9) || (this.day <= 22 && this.month == 10))
				type = HoroscopeMonthTypes.Libra;

			// Scorpio - Скорпион
			else if ((this.day > 22 && this.month == 10) || (this.day <= 22 && this.month == 11))
				type = HoroscopeMonthTypes.Scorpio;

			// Sagittarius - Стрелец
			else if ((this.day > 22 && this.month == 11) || (this.day <= 21 && this.month == 12))
				type = HoroscopeMonthTypes.Sagittarius;
		}
	}
}
