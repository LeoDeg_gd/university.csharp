﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeoDeg.DataGridViewExample
{
	public partial class MainWindow : Form
	{
		private int matrixSize = 0;

		public MainWindow ()
		{
			InitializeComponent ();
		}

		#region Events

		private void MainWindow_Load (object sender, EventArgs e)
		{
			dataGridViewMain.AllowUserToAddRows = false;
			dataGridViewDoubled.ReadOnly = true;
		}

		private void Button_SetRowsAndColumns_Click (object sender, EventArgs e)
		{
			GetUserMatrixSize ();

			dataGridViewMain.ColumnCount = matrixSize;
			dataGridViewMain.RowCount = matrixSize;
		}

		private void Button_DoubleValues_Click (object sender, EventArgs e)
		{
			dataGridViewDoubled.ColumnCount = matrixSize;
			dataGridViewDoubled.RowCount = matrixSize;
			DoubleSecondDataGridView ();
		}

		private void Button_GetMatrixSum_Click (object sender, EventArgs e)
		{
			AssignResultOfOperationToMatrix (AddTwoMatrices);
		}

		private void Button_GetMatrixProduct_Click (object sender, EventArgs e)
		{
			AssignResultOfOperationToMatrix (MultipliTwoMatrices);
		}

		private void Button_TransposeFirst_Click (object sender, EventArgs e)
		{
			TransposeDataGridView (dataGridViewMain);
		}

		private void Button_TransposeSecond_Click (object sender, EventArgs e)
		{
			TransposeDataGridView (dataGridViewDoubled);
		}

		private void Button_TransposeThird_Click (object sender, EventArgs e)
		{
			TransposeDataGridView (dataGridViewMath);
		}

		private void TextBox_RowsAndColumnsField_KeyPress (object sender, KeyPressEventArgs e)
		{
			base.OnKeyPress (e);
			OnKeyPressHandle (e);
		}

		private void DataGridViewMain_KeyPress (object sender, KeyPressEventArgs e)
		{
			base.OnKeyPress (e);
			OnKeyPressHandle (e);
		}

		private void DataGridViewMain_EditingControlShowing (object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			dataGridViewMain.EditingControl.KeyPress -= DataGridViewMain_KeyPress;
			dataGridViewMain.EditingControl.KeyPress += DataGridViewMain_KeyPress;
		}

		#endregion

		private void OnKeyPressHandle (KeyPressEventArgs e)
		{
			if ((Keys)e.KeyChar == Keys.Back || (Keys)e.KeyChar == Keys.Enter)
				return;

			if (char.IsDigit (e.KeyChar) == false)
				e.Handled = true;

			if (tbRowsAndColumnsField.Text.Length >= 2)
				e.Handled = true;
		}

		private void GetUserMatrixSize ()
		{
			try
			{
				matrixSize = int.Parse (tbRowsAndColumnsField.Text);
			}
			catch (ArgumentNullException ex)
			{
				MessageBox.Show (string.Format ("Argument Null Exception! \nError message: {0}", ex.Message));
			}
			catch (FormatException ex)
			{
				MessageBox.Show (string.Format ("Format Exception! \nError message: {0}", ex.Message));
			}
			catch (Exception ex)
			{
				MessageBox.Show (string.Format ("Error! \nError message: {0}", ex.Message));
			}
		}

		#region Matrix Operations Methods

		private void DoubleSecondDataGridView ()
		{
			int[,] doubledArray = new int[matrixSize, matrixSize];

			for (int i = 0; i < matrixSize; i++)
			{
				for (int j = 0; j < matrixSize; j++)
				{
					try
					{
						doubledArray[i, j] = int.Parse (dataGridViewMain.Rows[i].Cells[j].Value.ToString ());

						if (doubledArray[i, j] != default)
						{
							doubledArray[i, j] *= 2;
							dataGridViewDoubled.Rows[i].Cells[j].Value = doubledArray[i, j];
						}
					}
					catch (Exception ex)
					{
						MessageBox.Show (string.Format ("Row {0}, Cell {1}! \nError message: {2}", i, j, ex.Message));
					}
				}
			}
		}

		private void AssignResultOfOperationToMatrix (Func<DataGridView, DataGridView, int[,]> operation)
		{
			dataGridViewMath.ColumnCount = matrixSize;
			dataGridViewMath.RowCount = matrixSize;

			int[,] result = operation (dataGridViewMain, dataGridViewDoubled);

			for (int i = 0; i < matrixSize; i++)
				for (int j = 0; j < matrixSize; j++)
					dataGridViewMath.Rows[i].Cells[j].Value = result[i, j];
		}

		private int[,] AddTwoMatrices (DataGridView source, DataGridView other)
		{
			int[,] result = new int[matrixSize, matrixSize];

			for (int i = 0; i < matrixSize; i++)
			{
				for (int j = 0; j < matrixSize; j++)
				{
					try
					{
						int first = int.Parse (source.Rows[i].Cells[j].Value.ToString ());
						int second = int.Parse (other.Rows[i].Cells[j].Value.ToString ());

						result[i, j] = first + second;
						dataGridViewMath.Rows[i].Cells[j].Value = result[i, j];
					}
					catch (Exception ex)
					{
						MessageBox.Show (string.Format ("Row {0}, Cell {1}! \nError message: {2}", i, j, ex.Message));
					}
				}
			}

			return result;
		}

		private int[,] MultipliTwoMatrices (DataGridView source, DataGridView other)
		{
			int[,] result = new int[matrixSize, matrixSize];

			for (int i = 0; i < matrixSize; i++)
			{
				for (int j = 0; j < matrixSize; j++)
				{
					try
					{
						int first = int.Parse (source.Rows[i].Cells[j].Value.ToString ());
						int second = int.Parse (other.Rows[j].Cells[i].Value.ToString ());

						result[i, j] = first * second;
						dataGridViewMath.Rows[i].Cells[j].Value = result[i, j];
					}
					catch (Exception ex)
					{
						MessageBox.Show (string.Format ("Row {0}, Cell {1}! \nError message: {2}", i, j, ex.Message));
					}
				}
			}

			return result;
		}

		private void TransposeDataGridView (DataGridView dataGrid)
		{
			if (dataGrid.RowCount < 2 && dataGrid.ColumnCount < 2)
			{
				MessageBox.Show ("Data grid view must have more then 2 column and row.");
				return;
			}

			int[,] temp = GetTempArray (dataGrid);
			for (int i = 0; i < matrixSize; i++)
			{
				for (int j = 0; j < matrixSize; j++)
				{
					dataGrid.Rows[i].Cells[j].Value = temp[j, i];
				}
			}
		}

		private int[,] GetTempArray (DataGridView dataGrid)
		{
			int[,] temp = new int[matrixSize, matrixSize];
			for (int i = 0; i < matrixSize; i++)
			{
				for (int j = 0; j < matrixSize; j++)
				{
					try
					{
						temp[i, j] = int.Parse (dataGrid.Rows[i].Cells[j].Value.ToString ());
					}
					catch (Exception ex)
					{
						MessageBox.Show (string.Format ("Row {0}, Cell {1}! \nError message: {2}", i, j, ex.Message));
					}
				}
			}

			return temp;
		}

		#endregion
	}
}
