﻿namespace LeoDeg.DataGridViewExample
{
	partial class MainWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose ();
			}
			base.Dispose (disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
			this.dataGridViewMain = new System.Windows.Forms.DataGridView();
			this.dataGridViewDoubled = new System.Windows.Forms.DataGridView();
			this.tbRowsAndColumnsField = new System.Windows.Forms.TextBox();
			this.btnSetRowsAndColumns = new System.Windows.Forms.Button();
			this.btnDoubleValues = new System.Windows.Forms.Button();
			this.dataGridViewMath = new System.Windows.Forms.DataGridView();
			this.btnGetMatrixSum = new System.Windows.Forms.Button();
			this.btnGetMatrixProduct = new System.Windows.Forms.Button();
			this.btnTransposeFirst = new System.Windows.Forms.Button();
			this.btnTransposeSecond = new System.Windows.Forms.Button();
			this.btnTransposeThird = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewDoubled)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewMath)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridViewMain
			// 
			this.dataGridViewMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewMain.Location = new System.Drawing.Point(12, 12);
			this.dataGridViewMain.Name = "dataGridViewMain";
			this.dataGridViewMain.Size = new System.Drawing.Size(474, 210);
			this.dataGridViewMain.TabIndex = 0;
			this.dataGridViewMain.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DataGridViewMain_EditingControlShowing);
			this.dataGridViewMain.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DataGridViewMain_KeyPress);
			// 
			// dataGridViewDoubled
			// 
			this.dataGridViewDoubled.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewDoubled.Location = new System.Drawing.Point(12, 228);
			this.dataGridViewDoubled.Name = "dataGridViewDoubled";
			this.dataGridViewDoubled.Size = new System.Drawing.Size(474, 210);
			this.dataGridViewDoubled.TabIndex = 1;
			// 
			// tbRowsAndColumnsField
			// 
			this.tbRowsAndColumnsField.Font = new System.Drawing.Font("Microsoft Sans Serif", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.tbRowsAndColumnsField.Location = new System.Drawing.Point(508, 12);
			this.tbRowsAndColumnsField.Name = "tbRowsAndColumnsField";
			this.tbRowsAndColumnsField.Size = new System.Drawing.Size(192, 47);
			this.tbRowsAndColumnsField.TabIndex = 2;
			this.tbRowsAndColumnsField.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_RowsAndColumnsField_KeyPress);
			// 
			// btnSetRowsAndColumns
			// 
			this.btnSetRowsAndColumns.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnSetRowsAndColumns.Location = new System.Drawing.Point(508, 65);
			this.btnSetRowsAndColumns.Name = "btnSetRowsAndColumns";
			this.btnSetRowsAndColumns.Size = new System.Drawing.Size(192, 122);
			this.btnSetRowsAndColumns.TabIndex = 3;
			this.btnSetRowsAndColumns.Text = "Set the number of rows and columns";
			this.btnSetRowsAndColumns.UseVisualStyleBackColor = true;
			this.btnSetRowsAndColumns.Click += new System.EventHandler(this.Button_SetRowsAndColumns_Click);
			// 
			// btnDoubleValues
			// 
			this.btnDoubleValues.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnDoubleValues.Location = new System.Drawing.Point(508, 271);
			this.btnDoubleValues.Name = "btnDoubleValues";
			this.btnDoubleValues.Size = new System.Drawing.Size(192, 122);
			this.btnDoubleValues.TabIndex = 4;
			this.btnDoubleValues.Text = "Double the values";
			this.btnDoubleValues.UseVisualStyleBackColor = true;
			this.btnDoubleValues.Click += new System.EventHandler(this.Button_DoubleValues_Click);
			// 
			// dataGridViewMath
			// 
			this.dataGridViewMath.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewMath.Location = new System.Drawing.Point(12, 444);
			this.dataGridViewMath.Name = "dataGridViewMath";
			this.dataGridViewMath.Size = new System.Drawing.Size(474, 210);
			this.dataGridViewMath.TabIndex = 5;
			// 
			// btnGetMatrixSum
			// 
			this.btnGetMatrixSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnGetMatrixSum.Location = new System.Drawing.Point(508, 444);
			this.btnGetMatrixSum.Name = "btnGetMatrixSum";
			this.btnGetMatrixSum.Size = new System.Drawing.Size(192, 96);
			this.btnGetMatrixSum.TabIndex = 6;
			this.btnGetMatrixSum.Text = "Get Matrix Sum";
			this.btnGetMatrixSum.UseVisualStyleBackColor = true;
			this.btnGetMatrixSum.Click += new System.EventHandler(this.Button_GetMatrixSum_Click);
			// 
			// btnGetMatrixProduct
			// 
			this.btnGetMatrixProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnGetMatrixProduct.Location = new System.Drawing.Point(508, 558);
			this.btnGetMatrixProduct.Name = "btnGetMatrixProduct";
			this.btnGetMatrixProduct.Size = new System.Drawing.Size(192, 96);
			this.btnGetMatrixProduct.TabIndex = 7;
			this.btnGetMatrixProduct.Text = "Get Matrix Product";
			this.btnGetMatrixProduct.UseVisualStyleBackColor = true;
			this.btnGetMatrixProduct.Click += new System.EventHandler(this.Button_GetMatrixProduct_Click);
			// 
			// btnTransposeFirst
			// 
			this.btnTransposeFirst.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnTransposeFirst.Location = new System.Drawing.Point(12, 660);
			this.btnTransposeFirst.Name = "btnTransposeFirst";
			this.btnTransposeFirst.Size = new System.Drawing.Size(144, 65);
			this.btnTransposeFirst.TabIndex = 8;
			this.btnTransposeFirst.Text = "Transpose First";
			this.btnTransposeFirst.UseVisualStyleBackColor = true;
			this.btnTransposeFirst.Click += new System.EventHandler(this.Button_TransposeFirst_Click);
			// 
			// btnTransposeSecond
			// 
			this.btnTransposeSecond.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnTransposeSecond.Location = new System.Drawing.Point(178, 660);
			this.btnTransposeSecond.Name = "btnTransposeSecond";
			this.btnTransposeSecond.Size = new System.Drawing.Size(144, 65);
			this.btnTransposeSecond.TabIndex = 9;
			this.btnTransposeSecond.Text = "Transpose Second";
			this.btnTransposeSecond.UseVisualStyleBackColor = true;
			this.btnTransposeSecond.Click += new System.EventHandler(this.Button_TransposeSecond_Click);
			// 
			// btnTransposeThird
			// 
			this.btnTransposeThird.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnTransposeThird.Location = new System.Drawing.Point(342, 660);
			this.btnTransposeThird.Name = "btnTransposeThird";
			this.btnTransposeThird.Size = new System.Drawing.Size(144, 65);
			this.btnTransposeThird.TabIndex = 10;
			this.btnTransposeThird.Text = "Transpose Third";
			this.btnTransposeThird.UseVisualStyleBackColor = true;
			this.btnTransposeThird.Click += new System.EventHandler(this.Button_TransposeThird_Click);
			// 
			// MainWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(721, 737);
			this.Controls.Add(this.btnTransposeThird);
			this.Controls.Add(this.btnTransposeSecond);
			this.Controls.Add(this.btnTransposeFirst);
			this.Controls.Add(this.btnGetMatrixProduct);
			this.Controls.Add(this.btnGetMatrixSum);
			this.Controls.Add(this.dataGridViewMath);
			this.Controls.Add(this.btnDoubleValues);
			this.Controls.Add(this.btnSetRowsAndColumns);
			this.Controls.Add(this.tbRowsAndColumnsField);
			this.Controls.Add(this.dataGridViewDoubled);
			this.Controls.Add(this.dataGridViewMain);
			this.Name = "MainWindow";
			this.Text = "Data Grid View Example";
			this.Load += new System.EventHandler(this.MainWindow_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewDoubled)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewMath)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataGridView dataGridViewMain;
		private System.Windows.Forms.DataGridView dataGridViewDoubled;
		private System.Windows.Forms.TextBox tbRowsAndColumnsField;
		private System.Windows.Forms.Button btnSetRowsAndColumns;
		private System.Windows.Forms.Button btnDoubleValues;
		private System.Windows.Forms.DataGridView dataGridViewMath;
		private System.Windows.Forms.Button btnGetMatrixSum;
		private System.Windows.Forms.Button btnGetMatrixProduct;
		private System.Windows.Forms.Button btnTransposeFirst;
		private System.Windows.Forms.Button btnTransposeSecond;
		private System.Windows.Forms.Button btnTransposeThird;
	}
}

