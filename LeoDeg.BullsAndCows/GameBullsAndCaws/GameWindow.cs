﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameBullsAndCaws
{
	public partial class GameWindow : Form
	{
		Image cow;
		Image bull;
		Image wrong;

		int[] actualNumbers;
		const int MAX_NUMBERS = 4;
		enum Status { Wrong = 0, Cow = 1, Bull = 2 };

		public GameWindow ()
		{
			InitializeComponent ();
			actualNumbers = new int[MAX_NUMBERS];
		}

		#region Events

		private void GameWindow_Load (object sender, EventArgs e)
		{
			LoadImages ();
			StartNewGame ();
		}

		private void ToolStripNewGame_Click (object sender, EventArgs e)
		{
			StartNewGame ();
		}

		private void Button_Check_Click (object sender, EventArgs e)
		{
			StartCheckingResults ();
		}

		private void Button_Clear_Click (object sender, EventArgs e)
		{
			ClearStatus ();
			ClearPictureBoxes ();
			ClearTextBoxes ();
		}

		private void TextBox1_KeyPress (object sender, KeyPressEventArgs e)
		{
			base.OnKeyPress (e);
			OnHandleKeyPress (e, textBox1);
		}

		private void TextBox2_KeyPress (object sender, KeyPressEventArgs e)
		{
			base.OnKeyPress (e);
			OnHandleKeyPress (e, textBox2);
		}

		private void TextBox3_KeyPress (object sender, KeyPressEventArgs e)
		{
			base.OnKeyPress (e);
			OnHandleKeyPress (e, textBox3);
		}

		private void TextBox4_KeyPress (object sender, KeyPressEventArgs e)
		{
			base.OnKeyPress (e);
			OnHandleKeyPress (e, textBox4);
		}

		private static void OnHandleKeyPress (KeyPressEventArgs e, TextBox textBox)
		{

			if ((Keys)e.KeyChar == Keys.Enter || (Keys)e.KeyChar == Keys.Escape || (Keys)e.KeyChar == Keys.Back)
				return;

			if (textBox.Text.Length >= 1)
				e.Handled = true;

			if (!char.IsDigit (e.KeyChar))
				e.Handled = true;
		}

		#endregion Events

		#region Main Game Methods

		private void LoadImages ()
		{
			cow = Image.FromFile ("cow.jpg");
			bull = Image.FromFile ("bull.jpg");
			wrong = Image.FromFile ("wrong.jpg");
		}

		private void StartNewGame ()
		{
			GenerateNumbers ();
			ShowStatusMessage ("Game Begin!", Color.Green);
			ClearPictureBoxes ();
		}

		private void GenerateNumbers ()
		{
			Random generator = new Random ();
			for (int i = 0; i < actualNumbers.Length; i++)
			{
				bool generateNewNumber = true;
				while (generateNewNumber)
				{
					int generatedNumber = generator.Next (9);
					if (actualNumbers.Contains (generatedNumber) == false)
					{
						actualNumbers[i] = generatedNumber;
						generateNewNumber = false;
					}
				}
			}
		}

		private void ShowStatusMessage (string message, Color color)
		{
			lblStatus.ForeColor = (color == null) ? Color.Black : color;
			lblStatus.Text = string.Format ("Status: {0}", message);
		}

		#endregion Main Game Methods

		#region Check Results

		private void StartCheckingResults ()
		{
			try
			{
				int number1 = int.Parse (textBox1.Text);
				int number2 = int.Parse (textBox2.Text);
				int number3 = int.Parse (textBox3.Text);
				int number4 = int.Parse (textBox4.Text);

				AssignResultToPicture (pictureBox1, CheckResult (number1, 0));
				AssignResultToPicture (pictureBox2, CheckResult (number2, 1));
				AssignResultToPicture (pictureBox3, CheckResult (number3, 2));
				AssignResultToPicture (pictureBox4, CheckResult (number4, 3));
			}
			catch (Exception ex)
			{
				MessageBox.Show (ex.Message);
			}
		}

		private void AssignResultToPicture (PictureBox pictureBox, Status status)
		{
			pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;

			switch (status)
			{
				case Status.Wrong:
					pictureBox.Image = wrong;
					break;

				case Status.Cow:
					pictureBox.Image = cow;
					break;

				case Status.Bull:
					pictureBox.Image = bull;
					break;

				default:
					pictureBox.Image = null;
					break;
			}
		}

		private Status CheckResult (int result, int pictureBoxIndex)
		{
			for (int i = 0; i < actualNumbers.Length; i++)
				if (result == actualNumbers[i])
					return (pictureBoxIndex == i) ? Status.Bull : Status.Cow;

			return Status.Wrong;
		}

		#endregion Check Results

		#region Clear Info

		private void ClearStatus ()
		{
			lblStatus.ForeColor = Color.Black;
			lblStatus.Text = "Status: ";
		}

		private void ClearPictureBoxes ()
		{
			pictureBox1.Image = wrong;
			pictureBox2.Image = wrong;
			pictureBox3.Image = wrong;
			pictureBox4.Image = wrong;
		}

		private void ClearTextBoxes ()
		{
			textBox1.Text = string.Empty;
			textBox2.Text = string.Empty;
			textBox3.Text = string.Empty;
			textBox4.Text = string.Empty;
		}

		#endregion Clear Info
	}
}
