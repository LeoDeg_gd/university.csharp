﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncryptionForms
{
	public static class CharExtensionMethods
	{
		public static bool IsEnglish (this char character)
		{
			if (character >= 97 && character <= 122 // Upper case
				|| character >= 65 && character <= 90) // Lower case
				return true;
			return false;
		}

		public static bool IsEnglishOrWhiteSpace (this char character)
		{
			if (character >= 97 && character <= 122 // Upper case
				|| character >= 65 && character <= 90 // Lower case
				|| character == 32) // White space
				return true;
			return false;
		}

		public static bool IsEnglishOrSymbols (this char character)
		{
			if (character >= 32 && character <= 126)
				return true;
			return false;
		}
	}
}
