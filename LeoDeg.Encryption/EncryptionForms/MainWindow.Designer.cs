﻿namespace EncryptionForms
{
	partial class MainWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose ();
			}
			base.Dispose (disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
			this.labelTitle = new System.Windows.Forms.Label();
			this.textBoxText = new System.Windows.Forms.TextBox();
			this.buttonEncode = new System.Windows.Forms.Button();
			this.buttonDecode = new System.Windows.Forms.Button();
			this.comboBoxKey = new System.Windows.Forms.ComboBox();
			this.labelKey = new System.Windows.Forms.Label();
			this.buttonSaveToOutput = new System.Windows.Forms.Button();
			this.buttonSaveToInput = new System.Windows.Forms.Button();
			this.buttonLoadFromOutput = new System.Windows.Forms.Button();
			this.buttonLoadFromInput = new System.Windows.Forms.Button();
			this.buttonOpenOutputFile = new System.Windows.Forms.Button();
			this.buttonOpenInputFile = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// labelTitle
			// 
			this.labelTitle.AutoSize = true;
			this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelTitle.Location = new System.Drawing.Point(113, 32);
			this.labelTitle.Name = "labelTitle";
			this.labelTitle.Size = new System.Drawing.Size(215, 25);
			this.labelTitle.TabIndex = 0;
			this.labelTitle.Text = "Julius Caesar Cipher";
			// 
			// textBoxText
			// 
			this.textBoxText.Location = new System.Drawing.Point(12, 72);
			this.textBoxText.Multiline = true;
			this.textBoxText.Name = "textBoxText";
			this.textBoxText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBoxText.Size = new System.Drawing.Size(410, 213);
			this.textBoxText.TabIndex = 1;
			this.textBoxText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_Text_KeyPress);
			// 
			// buttonEncode
			// 
			this.buttonEncode.Location = new System.Drawing.Point(13, 371);
			this.buttonEncode.Name = "buttonEncode";
			this.buttonEncode.Size = new System.Drawing.Size(192, 78);
			this.buttonEncode.TabIndex = 2;
			this.buttonEncode.Text = "Encode";
			this.buttonEncode.UseVisualStyleBackColor = true;
			this.buttonEncode.Click += new System.EventHandler(this.Button_Encode_Click);
			// 
			// buttonDecode
			// 
			this.buttonDecode.Location = new System.Drawing.Point(230, 371);
			this.buttonDecode.Name = "buttonDecode";
			this.buttonDecode.Size = new System.Drawing.Size(192, 78);
			this.buttonDecode.TabIndex = 3;
			this.buttonDecode.Text = "Decode";
			this.buttonDecode.UseVisualStyleBackColor = true;
			this.buttonDecode.Click += new System.EventHandler(this.Button_Decode_Click);
			// 
			// comboBoxKey
			// 
			this.comboBoxKey.FormattingEnabled = true;
			this.comboBoxKey.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
			this.comboBoxKey.Location = new System.Drawing.Point(173, 323);
			this.comboBoxKey.Name = "comboBoxKey";
			this.comboBoxKey.Size = new System.Drawing.Size(89, 21);
			this.comboBoxKey.TabIndex = 5;
			this.comboBoxKey.SelectedIndexChanged += new System.EventHandler(this.ComboBoxKey_SelectedIndexChanged);
			// 
			// labelKey
			// 
			this.labelKey.AutoSize = true;
			this.labelKey.Location = new System.Drawing.Point(209, 307);
			this.labelKey.Name = "labelKey";
			this.labelKey.Size = new System.Drawing.Size(25, 13);
			this.labelKey.TabIndex = 6;
			this.labelKey.Text = "Key";
			// 
			// buttonSaveToOutput
			// 
			this.buttonSaveToOutput.Location = new System.Drawing.Point(230, 470);
			this.buttonSaveToOutput.Name = "buttonSaveToOutput";
			this.buttonSaveToOutput.Size = new System.Drawing.Size(192, 36);
			this.buttonSaveToOutput.TabIndex = 8;
			this.buttonSaveToOutput.Text = "Save To Output";
			this.buttonSaveToOutput.UseVisualStyleBackColor = true;
			this.buttonSaveToOutput.Click += new System.EventHandler(this.Button_SaveToOutput_Click);
			// 
			// buttonSaveToInput
			// 
			this.buttonSaveToInput.Location = new System.Drawing.Point(13, 470);
			this.buttonSaveToInput.Name = "buttonSaveToInput";
			this.buttonSaveToInput.Size = new System.Drawing.Size(192, 36);
			this.buttonSaveToInput.TabIndex = 7;
			this.buttonSaveToInput.Text = "Save To Input";
			this.buttonSaveToInput.UseVisualStyleBackColor = true;
			this.buttonSaveToInput.Click += new System.EventHandler(this.Button_SaveToInput_Click);
			// 
			// buttonLoadFromOutput
			// 
			this.buttonLoadFromOutput.Location = new System.Drawing.Point(230, 512);
			this.buttonLoadFromOutput.Name = "buttonLoadFromOutput";
			this.buttonLoadFromOutput.Size = new System.Drawing.Size(192, 36);
			this.buttonLoadFromOutput.TabIndex = 10;
			this.buttonLoadFromOutput.Text = "Load From Output";
			this.buttonLoadFromOutput.UseVisualStyleBackColor = true;
			this.buttonLoadFromOutput.Click += new System.EventHandler(this.Button_LoadFromOutput_Click);
			// 
			// buttonLoadFromInput
			// 
			this.buttonLoadFromInput.Location = new System.Drawing.Point(13, 512);
			this.buttonLoadFromInput.Name = "buttonLoadFromInput";
			this.buttonLoadFromInput.Size = new System.Drawing.Size(192, 36);
			this.buttonLoadFromInput.TabIndex = 9;
			this.buttonLoadFromInput.Text = "Load From Input";
			this.buttonLoadFromInput.UseVisualStyleBackColor = true;
			this.buttonLoadFromInput.Click += new System.EventHandler(this.Button_LoadFromInput_Click);
			// 
			// buttonOpenOutputFile
			// 
			this.buttonOpenOutputFile.Location = new System.Drawing.Point(230, 554);
			this.buttonOpenOutputFile.Name = "buttonOpenOutputFile";
			this.buttonOpenOutputFile.Size = new System.Drawing.Size(192, 36);
			this.buttonOpenOutputFile.TabIndex = 12;
			this.buttonOpenOutputFile.Text = "Open Output File";
			this.buttonOpenOutputFile.UseVisualStyleBackColor = true;
			this.buttonOpenOutputFile.Click += new System.EventHandler(this.Button_OpenOutputFile_Click);
			// 
			// buttonOpenInputFile
			// 
			this.buttonOpenInputFile.Location = new System.Drawing.Point(13, 554);
			this.buttonOpenInputFile.Name = "buttonOpenInputFile";
			this.buttonOpenInputFile.Size = new System.Drawing.Size(192, 36);
			this.buttonOpenInputFile.TabIndex = 11;
			this.buttonOpenInputFile.Text = "Open Input File";
			this.buttonOpenInputFile.UseVisualStyleBackColor = true;
			this.buttonOpenInputFile.Click += new System.EventHandler(this.Button_OpenInputFile_Click);
			// 
			// MainWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(434, 606);
			this.Controls.Add(this.buttonOpenOutputFile);
			this.Controls.Add(this.buttonOpenInputFile);
			this.Controls.Add(this.buttonLoadFromOutput);
			this.Controls.Add(this.buttonLoadFromInput);
			this.Controls.Add(this.buttonSaveToOutput);
			this.Controls.Add(this.buttonSaveToInput);
			this.Controls.Add(this.labelKey);
			this.Controls.Add(this.comboBoxKey);
			this.Controls.Add(this.buttonDecode);
			this.Controls.Add(this.buttonEncode);
			this.Controls.Add(this.textBoxText);
			this.Controls.Add(this.labelTitle);
			this.Name = "MainWindow";
			this.Text = "Julius Caesar Cipher";
			this.Load += new System.EventHandler(this.MainWindow_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label labelTitle;
		private System.Windows.Forms.TextBox textBoxText;
		private System.Windows.Forms.Button buttonEncode;
		private System.Windows.Forms.Button buttonDecode;
		private System.Windows.Forms.ComboBox comboBoxKey;
		private System.Windows.Forms.Label labelKey;
		private System.Windows.Forms.Button buttonSaveToOutput;
		private System.Windows.Forms.Button buttonSaveToInput;
		private System.Windows.Forms.Button buttonLoadFromOutput;
		private System.Windows.Forms.Button buttonLoadFromInput;
		private System.Windows.Forms.Button buttonOpenOutputFile;
		private System.Windows.Forms.Button buttonOpenInputFile;
	}
}

