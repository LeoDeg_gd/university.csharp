﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EncryptionForms
{
	public partial class MainWindow : Form
	{
		private const string INPUT_FILE_NAME = "input.txt";
		private const string OUTPUT_FILE_NAME = "output.txt";

		private EncryptionCore.JuliusCaesarEncryption caesarEncryption;

		public MainWindow ()
		{
			InitializeComponent ();
		}

		private void MainWindow_Load (object sender, System.EventArgs e)
		{
			comboBoxKey.SelectedIndex = 2;
		}

		private void TextBox_Text_KeyPress (object sender, KeyPressEventArgs e)
		{
			base.OnKeyPress (e);

			if ((Keys)e.KeyChar == Keys.Escape || (Keys)e.KeyChar == Keys.Enter)
				return;

			if (!e.KeyChar.IsEnglishOrSymbols ())
			{
				MessageBox.Show ("Warning: Text must contain only English letters.");
				e.Handled = true;
			}
		}

		private void ComboBoxKey_SelectedIndexChanged (object sender, System.EventArgs e)
		{
			caesarEncryption = new EncryptionCore.JuliusCaesarEncryption (comboBoxKey.SelectedIndex + 1);
		}

		private void Button_Encode_Click (object sender, EventArgs e)
		{
			AssignEncryptedInformation (caesarEncryption.Encode);
		}

		private void Button_Decode_Click (object sender, EventArgs e)
		{
			AssignEncryptedInformation (caesarEncryption.Decode);
		}

		private void AssignEncryptedInformation (Func<string, string> encryptMethod)
		{
			if (textBoxText.TextLength < 0)
			{
				MessageBox.Show ("Warning: Text is empty.");
				return;
			}

			textBoxText.Text = encryptMethod (textBoxText.Text);
		}

		private void Button_SaveToInput_Click (object sender, EventArgs e)
		{
			SaveToFile (INPUT_FILE_NAME);
		}

		private void Button_SaveToOutput_Click (object sender, EventArgs e)
		{
			SaveToFile (OUTPUT_FILE_NAME);
		}

		private void SaveToFile (string fileName)
		{
			try
			{
				File.WriteAllText (fileName, string.Empty);
				using (StreamWriter streamWriter = File.AppendText (fileName))
				{
					foreach (var item in textBoxText.Text)
					{
						streamWriter.Write (item);
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show (string.Format ("Error: {0}", ex.Message));
			}
		}

		private void Button_LoadFromInput_Click (object sender, EventArgs e)
		{
			LoadFromFile (INPUT_FILE_NAME);
		}

		private void Button_LoadFromOutput_Click (object sender, EventArgs e)
		{
			LoadFromFile (OUTPUT_FILE_NAME);
		}

		private void LoadFromFile (string fileName)
		{
			try
			{
				textBoxText.Text = File.ReadAllText (fileName);
			}
			catch (Exception ex)
			{
				MessageBox.Show (string.Format ("Error: {0}", ex.Message));
			}
		}

		private void Button_OpenInputFile_Click (object sender, EventArgs e)
		{
			Process.Start (INPUT_FILE_NAME);
		}

		private void Button_OpenOutputFile_Click (object sender, EventArgs e)
		{
			Process.Start (OUTPUT_FILE_NAME);
		}
	}
}
