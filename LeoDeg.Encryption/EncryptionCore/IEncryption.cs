﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncryptionCore
{

	public interface IEncryption
	{
		string Encode (string source);
		string Decode (string source);
	}
}
