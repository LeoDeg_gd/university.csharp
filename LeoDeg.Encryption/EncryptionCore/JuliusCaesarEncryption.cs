﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncryptionCore
{
	/* Julius Caesar Cipher
	 * y = (x + k) mod n
	 * x = (y - k) mod n
	 * 
	 * where: 
	 * y - encoded character, 
	 * x - decoded character, 
	 * k - key, 
	 * n - amount characters of an alphabet
	 * 
	 * ASCII:
	 * A - 65, Z - 90
	 * a - 97, z - 122
	 */

	public class JuliusCaesarEncryption : IEncryption
	{
		private const int ENGLISH_ALPHABET_COUNT = 26;
		private int key;

		public JuliusCaesarEncryption (int key)
		{
			this.key = key;
		}

		public string Encode (string source)
		{
			return EncryptString (source, this.key);
		}

		public string Decode (string source)
		{
			/* A = 1 => A + 3 => D = 4
			 * 26 - 3 = 23
			 * 4 + 23 = 27 => 27 mod 26 = 1 => A = 1 
			 */
			 return EncryptString (source, ENGLISH_ALPHABET_COUNT - this.key);
		}

		private string EncryptString (string source, int key)
		{
			StringBuilder stringBuilder = new StringBuilder ();

			foreach (char item in source)
				stringBuilder.Append (EncryptCharacter (item, key));

			return stringBuilder.ToString ();
		}

		private char EncryptCharacter (char character, int key)
		{
			if (!char.IsLetter (character))
				return character;

			char startLetter = char.IsUpper (character) ? 'A' : 'a';
			return (char)(((character + key - startLetter) % 26) + startLetter);
		}
	}
}
