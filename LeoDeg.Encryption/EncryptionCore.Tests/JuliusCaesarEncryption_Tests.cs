using NUnit.Framework;
using EncryptionCore;

namespace EncryptionCore.Tests
{
	public class JuliusCaesarEncryption_Tests
	{
		private JuliusCaesarEncryption encryption;

		[SetUp]
		public void Setup ()
		{
			encryption = new JuliusCaesarEncryption (3);
		}

		[Test]
		public void JuliusCaesarEncryption_EncodingString_ReturnEncodedString ()
		{
			string actual = encryption.Encode ("Hello world");
			string expected = "Khoor zruog";

			Assert.AreEqual (expected, actual);
		}

		[Test]
		public void JuliusCaesarEncryption_DecodingString_ReturnDecodedString ()
		{
			string actual = encryption.Decode ("Khoor zruog");
			string expected = "Hello world";

			Assert.AreEqual (expected, actual);
		}
	}
}