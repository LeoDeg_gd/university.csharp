﻿using System;

namespace LeoDeg.Lab_001
{
    class Program
    {
        static void Main (string[] args)
        {
            DisplayOperationSystemInformation ();
            Console.WriteLine ();
            StartCalculationProcess ();

            Console.ReadKey ();
        }

        private static void DisplayOperationSystemInformation ()
        {
            OperatingSystem operatingSystem = Environment.OSVersion;
            Console.WriteLine ("Current OS platform: {0}", operatingSystem.Platform);
            Console.WriteLine ("Current OS version: {0}", operatingSystem.VersionString);
            Console.WriteLine ("Current time: {0}", DateTime.Now);
        }

        private static void StartCalculationProcess ()
        {
            try
            {
                double x = ReadDoubleNumberFromConsole ("x");
                Console.WriteLine ("First formula calculation result is: {0}", Calculate (x));
                Console.WriteLine ("Second formula calculation result is: {0}", Calculate2 (x));
                Console.WriteLine ("Third formula calculation result is: {0}", Calculate3 (x));
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine ("Error: white space. \n{0}", ex.Message);
            }
            catch (FormatException ex)
            {
                Console.WriteLine ("Error: value is not a number. \n{0}", ex.Message);
            }
            catch (OverflowException ex)
            {
                Console.WriteLine ("Error: value is too big. Value must be in diapason: {0} to {1}. \n{0}", double.MinValue, double.MaxValue, ex.Message);
            }
        }

        private static double ReadDoubleNumberFromConsole (string valueName)
        {
            Console.Write ("Enter {0}: ", valueName);
            return double.Parse (Console.ReadLine ());
        }

        private static double Calculate (double x)
        {
            return Math.Log (5 / 3 + x) - Math.Sin (Math.Sqrt (7 / 3 + x)) + 1 / 7; // 26 variant 
        }

        private static double Calculate2 (double x)
        {
            return Math.Cos (x / 3) - Math.Pow (Math.E, Math.Pow (-(x + 1), 2)) - 4 / 9; // 29 variant
        }

        private static double Calculate3 (double x)
        {
            return Math.Cos (1.5) * x - Math.Pow (Math.E, Math.Sin (x + 5 / 3)) + Math.Sqrt (x + 7 / 6); // 14 variant
        }
    }
}
