﻿using StudentsInformation;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace StudentsInformationForms
{
	public partial class MainWindow : Form
	{
		private StudentsManagementSystem managementSystem;
		private SortInformationForm sortInformationForm;
		private AddStudentForm addStudentInfoForm;
		private ShowInfoForm showStudentsInfo;
		private ShowInfoForm showLogsInfo;
		private ExamsForm examsForm;
		private ExamsForm testsForm;

		public MainWindow ()
		{
			InitializeComponent ();
		}

		private void StudentsInformationGrid_Load (object sender, EventArgs e)
		{
			managementSystem = new StudentsManagementSystem ();
			ReloadAndShowStudentsInformation ();
			InitializeForms ();
		}

		private void InitializeForms ()
		{
			addStudentInfoForm = new AddStudentForm ();
			addStudentInfoForm.OnAddStudent += managementSystem.StudentsCollection.Add;
			addStudentInfoForm.OnAddedStudent += ShowStudentsInformation;

			examsForm = new ExamsForm ();
			examsForm.Text = "Exams List";
			examsForm.OnSaveInfo += ShowStudentsInformation;

			testsForm = new ExamsForm ();
			testsForm.Text = "Tests List";
			testsForm.OnSaveInfo += ShowStudentsInformation;

			showStudentsInfo = new ShowInfoForm ();
			showStudentsInfo.OnShowInfo += managementSystem.StudentsCollection.ToString;

			showLogsInfo = new ShowInfoForm ();
			showLogsInfo.OnShowInfo += managementSystem.Logs.ToString;

			sortInformationForm = new SortInformationForm ();
			sortInformationForm.OnSort += ShowStudentsInformation;
			sortInformationForm.OnSortByFirstName += managementSystem.StudentsCollection.Sort;
			sortInformationForm.OnSortByLastName += managementSystem.StudentsCollection.Sort;
			sortInformationForm.OnSortByDateOfBirth += managementSystem.StudentsCollection.Sort;
			sortInformationForm.OnSortByExamsAverageRating += managementSystem.StudentsCollection.Sort;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose ();
			}
			base.Dispose (disposing);
			DisposeForms ();

		}

		private void DisposeForms ()
		{
			printPreviewDialog.Dispose ();
			addStudentInfoForm.Dispose ();
			examsForm.Dispose ();
			testsForm.Dispose ();
			showStudentsInfo.Dispose ();
			showLogsInfo.Dispose ();
			sortInformationForm.Dispose ();
		}

		private void StudentsInformationGrid_FormClosing (object sender, FormClosingEventArgs e)
		{
			DialogResult dialogResult = MessageBox.Show ("Save information?", "Save information", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

			switch (dialogResult)
			{
				case DialogResult.Cancel:
					e.Cancel = (dialogResult == DialogResult.Cancel);
					break;

				case DialogResult.Yes:
					SaveInformationToXMLFIle ();
					managementSystem.Logs.SaveToFileAndClear ();
					break;

				case DialogResult.No:
					managementSystem.Logs.SaveToFileAndClear ();
					break;

				default: throw new ArgumentException ();
			}
		}

		#region Mouse Click Events

		private void MainWindow_MouseClick (object sender, MouseEventArgs e)
		{
			OnMouseRightClick (e);
		}


		private void dataGridViewStudentsInfo_MouseClick (object sender, MouseEventArgs e)
		{
			OnMouseRightClick (e);
		}

		private void OnMouseRightClick (MouseEventArgs e)
		{
			switch (e.Button)
			{
				case MouseButtons.Right:
					contextMenuStrip.Show (this, e.Location);
					break;

				default: return;
			}
		}

		#endregion

		#region Buttons Events

		private void Button_AddStudent_Click (object sender, EventArgs e)
		{
			new Thread (() => addStudentInfoForm.ShowDialog ()).Start ();
		}

		private void Button_Update_Click (object sender, EventArgs e)
		{
			ShowStudentsInformation ();
		}

		private void Button_Save_Click (object sender, EventArgs e)
		{
			SaveInformationToXMLFIle ();
		}

		private void Button_Reload_Click (object sender, EventArgs e)
		{
			ReloadAndShowStudentsInformation ();
		}

		private void Button_ShowStudentsInfo_Click (object sender, EventArgs e)
		{
			new Thread (() => showStudentsInfo.ShowDialog ()).Start ();
		}

		private void Button_Sort_Click (object sender, EventArgs e)
		{
			new Thread (() => sortInformationForm.ShowDialog ()).Start ();
		}

		private void Button_OpenLogs_Click (object sender, EventArgs e)
		{
			Process.Start ("Logs");
		}

		private void Button_OpenFile_Click (object sender, EventArgs e)
		{
			Process.Start (StudentsXMLInfo.FILE_PATH);
		}

		private void Button_Print_Click (object sender, EventArgs e)
		{
			OnPrintButtonClick ();
		}

		#endregion

		#region Menu Strip Events

		private void Save_ToolStripMenuItem_Click (object sender, EventArgs e)
		{
			SaveInformationToXMLFIle ();
		}

		private void Reload_ToolStripMenuItem_Click (object sender, EventArgs e)
		{
			ReloadAndShowStudentsInformation ();
		}

		private void OpenLogsDirectory_ToolStripMenuItem_Click (object sender, EventArgs e)
		{
			Process.Start ("Logs");
		}

		private void OpenInformationFile_ToolStripMenuItem_Click (object sender, EventArgs e)
		{
			Process.Start (StudentsXMLInfo.FILE_PATH);
		}

		private void ShowStudentsInfo_ToolStripMenuItem_Click (object sender, EventArgs e)
		{
			new Thread (() => showStudentsInfo.ShowDialog ()).Start ();
		}

		private void ShowLogsInfo_ToolStripMenuItem_Click (object sender, EventArgs e)
		{
			new Thread (() => showLogsInfo.ShowDialog ()).Start ();
		}

		private void AddStudent_ToolStripMenuItem_Click (object sender, EventArgs e)
		{
			new Thread (() => addStudentInfoForm.ShowDialog ()).Start ();
		}

		private void Update_ToolStripMenuItem_Click (object sender, EventArgs e)
		{
			ShowStudentsInformation ();
		}

		private void Sort_ToolStripMenuItem_Click (object sender, EventArgs e)
		{
			new Thread (() => sortInformationForm.ShowDialog ()).Start ();
		}

		private void Print_ToolStripMenuItem1_Click (object sender, EventArgs e)
		{
			OnPrintButtonClick ();
		}

		#endregion

		#region Tool Strip Menu Events

		private void ToolStripMenuItem_Save_Click (object sender, EventArgs e)
		{
			SaveInformationToXMLFIle ();
		}

		private void ToolStripMenuItem_Reload_Click (object sender, EventArgs e)
		{
			ReloadAndShowStudentsInformation ();
		}

		private void ToolStripMenuItem_SortTable_Click (object sender, EventArgs e)
		{
			new Thread (() => sortInformationForm.ShowDialog ()).Start ();
		}

		private void ToolStripMenuItem_UpdateTable_Click (object sender, EventArgs e)
		{
			ShowStudentsInformation ();
		}

		#endregion

		#region Print Events Methods

		private void OnPrintButtonClick ()
		{
			printPreviewDialog.Document = printDocument;
			printPreviewDialog.ShowDialog ();
		}

		private void PrintDocument_PrintPage (object sender, System.Drawing.Printing.PrintPageEventArgs e)
		{
			Bitmap bm = new Bitmap (dataGridViewStudentsInfo.Width, dataGridViewStudentsInfo.Height);
			dataGridViewStudentsInfo.DrawToBitmap (bm, new Rectangle (0, 0, dataGridViewStudentsInfo.Width, dataGridViewStudentsInfo.Height));
			e.Graphics.DrawImage (bm, 0, 0);
		}

		private void DataGridViewStudentsInfo_CellContentClick (object sender, DataGridViewCellEventArgs e)
		{
			if (dataGridViewStudentsInfo.Columns[e.ColumnIndex] is DataGridViewButtonColumn button && e.RowIndex >= 0)
			{
				OnShowListOfExamsFormButtonClick (e, button);
			}
		}

		#endregion

		private void SaveInformationToXMLFIle ()
		{
			try
			{
				managementSystem.SaveInformationToXMLFile ();
			}
			catch (Exception ex)
			{
				MessageBox.Show (ex.Message);
			}
		}

		private void ReloadAndShowStudentsInformation ()
		{
			try
			{
				managementSystem.ReloadInformationFromXMLFile ();
				ShowStudentsInformation ();
			}
			catch (Exception ex)
			{
				MessageBox.Show (ex.Message);
			}
		}

		#region Display Information Methods

		private void ShowStudentsInformation ()
		{
			if (managementSystem.StudentsCollection.Count < 1)
			{
				MessageBox.Show ("Students information is empty.\nPlease add a new information to database.");
				return;
			}

			labelStudentsAmount.Text = "Amount: " + managementSystem.StudentsCollection.Count;
			DisplayStudentsInformationToGridView ();
		}

		private void DisplayStudentsInformationToGridView ()
		{
			int rowIndex = 0;
			foreach (Student student in managementSystem.StudentsCollection)
			{
				if (dataGridViewStudentsInfo.Rows.Count < managementSystem.StudentsCollection.Count)
					dataGridViewStudentsInfo.Rows.Add ();

				DisplayStudentInformationToGridView (rowIndex, student);
				++rowIndex;
			}
		}

		private void DisplayStudentInformationToGridView (int rowIndex, Student student)
		{
			dataGridViewStudentsInfo.Rows[rowIndex].Cells[0].Value = student.GroupID.ToString ();
			dataGridViewStudentsInfo.Rows[rowIndex].Cells[1].Value = student.FirstName;
			dataGridViewStudentsInfo.Rows[rowIndex].Cells[2].Value = student.LastName;
			dataGridViewStudentsInfo.Rows[rowIndex].Cells[3].Value = student.DateOfBirth.ToShortDateString ();
			dataGridViewStudentsInfo.Rows[rowIndex].Cells[4].Value = student.Education.ToString ();
			dataGridViewStudentsInfo.Rows[rowIndex].Cells[5].Value = "Show";
			dataGridViewStudentsInfo.Rows[rowIndex].Cells[6].Value = "Show";
			dataGridViewStudentsInfo.Rows[rowIndex].Cells[7].Value = "Delete";
		}

		private void OnShowListOfExamsFormButtonClick (DataGridViewCellEventArgs e, DataGridViewButtonColumn button)
		{
			Student student = FindStudentByGridInfo (e.RowIndex);

			if (button.Name == "Exams")
			{
				examsForm.Exams = student.Exams;
				examsForm.DisplayStudentInfo (student.FirstName, student.LastName, student.GroupID);
				examsForm.ShowDialog ();
			}
			else if (button.Name == "Tests")
			{
				testsForm.Exams = student.Tests;
				testsForm.DisplayStudentInfo (student.FirstName, student.LastName, student.GroupID);
				testsForm.ShowDialog ();
			}
			else if (button.Name == "Delete")
			{
				managementSystem.StudentsCollection.Remove (student);
				dataGridViewStudentsInfo.Rows.Clear ();
				ShowStudentsInformation ();
			}
		}

		private Student FindStudentByGridInfo (int rowIndex)
		{
			return managementSystem.StudentsCollection.FindStudent (
				GetCellValue (rowIndex, 0).ToString (),
				GetCellValue (rowIndex, 1).ToString (),
				GetCellValue (rowIndex, 2).ToString (),
				GetCellValue (rowIndex, 3).ToString ()
			);
		}

		private object GetCellValue (int rowIndex, int columnIndex)
		{
			return dataGridViewStudentsInfo.Rows[rowIndex].Cells[columnIndex].Value;
		}


		#endregion
	}
}
