﻿namespace StudentsInformationForms
{
	partial class SortInformationForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose ();
			}
			base.Dispose (disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
			this.buttonSortByFirstName = new System.Windows.Forms.Button();
			this.buttonSortByLastName = new System.Windows.Forms.Button();
			this.buttonSortByDateOfBirth = new System.Windows.Forms.Button();
			this.buttonSortByExamsAverageRating = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// buttonSortByFirstName
			// 
			this.buttonSortByFirstName.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonSortByFirstName.Location = new System.Drawing.Point(12, 12);
			this.buttonSortByFirstName.Name = "buttonSortByFirstName";
			this.buttonSortByFirstName.Size = new System.Drawing.Size(217, 77);
			this.buttonSortByFirstName.TabIndex = 7;
			this.buttonSortByFirstName.Text = "Sort by First Name";
			this.buttonSortByFirstName.UseVisualStyleBackColor = true;
			this.buttonSortByFirstName.Click += new System.EventHandler(this.Button_SortByFirstName_Click);
			// 
			// buttonSortByLastName
			// 
			this.buttonSortByLastName.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonSortByLastName.Location = new System.Drawing.Point(12, 95);
			this.buttonSortByLastName.Name = "buttonSortByLastName";
			this.buttonSortByLastName.Size = new System.Drawing.Size(217, 77);
			this.buttonSortByLastName.TabIndex = 8;
			this.buttonSortByLastName.Text = "Sort by Last Name";
			this.buttonSortByLastName.UseVisualStyleBackColor = true;
			this.buttonSortByLastName.Click += new System.EventHandler(this.Button_SortByLastName_Click);
			// 
			// buttonSortByDateOfBirth
			// 
			this.buttonSortByDateOfBirth.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonSortByDateOfBirth.Location = new System.Drawing.Point(12, 178);
			this.buttonSortByDateOfBirth.Name = "buttonSortByDateOfBirth";
			this.buttonSortByDateOfBirth.Size = new System.Drawing.Size(217, 77);
			this.buttonSortByDateOfBirth.TabIndex = 9;
			this.buttonSortByDateOfBirth.Text = "Sort by Date of birth";
			this.buttonSortByDateOfBirth.UseVisualStyleBackColor = true;
			this.buttonSortByDateOfBirth.Click += new System.EventHandler(this.Button_SortByDateOfBirth_Click);
			// 
			// buttonSortByExamsAverageRating
			// 
			this.buttonSortByExamsAverageRating.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonSortByExamsAverageRating.Location = new System.Drawing.Point(12, 261);
			this.buttonSortByExamsAverageRating.Name = "buttonSortByExamsAverageRating";
			this.buttonSortByExamsAverageRating.Size = new System.Drawing.Size(217, 77);
			this.buttonSortByExamsAverageRating.TabIndex = 10;
			this.buttonSortByExamsAverageRating.Text = "Sort by Exams Average Rating";
			this.buttonSortByExamsAverageRating.UseVisualStyleBackColor = true;
			this.buttonSortByExamsAverageRating.Click += new System.EventHandler(this.Button_SortByExamsAverageRating_Click);
			// 
			// SortInformationForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(241, 358);
			this.Controls.Add(this.buttonSortByExamsAverageRating);
			this.Controls.Add(this.buttonSortByDateOfBirth);
			this.Controls.Add(this.buttonSortByLastName);
			this.Controls.Add(this.buttonSortByFirstName);
			this.Name = "SortInformationForm";
			this.ShowIcon = false;
			this.Text = "Sort Information";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button buttonSortByFirstName;
		private System.Windows.Forms.Button buttonSortByLastName;
		private System.Windows.Forms.Button buttonSortByDateOfBirth;
		private System.Windows.Forms.Button buttonSortByExamsAverageRating;
	}
}