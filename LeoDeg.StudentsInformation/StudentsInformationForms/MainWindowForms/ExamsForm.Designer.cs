﻿namespace StudentsInformationForms
{
	partial class ExamsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose ();
			}
			base.Dispose (disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this.dataGridViewExams = new System.Windows.Forms.DataGridView();
			this.ExamName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Mark = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.DateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.textBoxExamName = new System.Windows.Forms.TextBox();
			this.labelExamName = new System.Windows.Forms.Label();
			this.labelMark = new System.Windows.Forms.Label();
			this.labelDateTime = new System.Windows.Forms.Label();
			this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
			this.buttonAdd = new System.Windows.Forms.Button();
			this.comboBoxMark = new System.Windows.Forms.ComboBox();
			this.labelStudentInfo = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewExams)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridViewExams
			// 
			this.dataGridViewExams.AllowUserToAddRows = false;
			this.dataGridViewExams.AllowUserToDeleteRows = false;
			this.dataGridViewExams.AllowUserToOrderColumns = true;
			this.dataGridViewExams.AllowUserToResizeRows = false;
			this.dataGridViewExams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dataGridViewExams.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dataGridViewExams.BackgroundColor = System.Drawing.Color.White;
			this.dataGridViewExams.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewExams.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dataGridViewExams.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewExams.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ExamName,
            this.Mark,
            this.DateTime});
			this.dataGridViewExams.Location = new System.Drawing.Point(12, 134);
			this.dataGridViewExams.Name = "dataGridViewExams";
			this.dataGridViewExams.RowHeadersVisible = false;
			this.dataGridViewExams.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.dataGridViewExams.Size = new System.Drawing.Size(550, 477);
			this.dataGridViewExams.TabIndex = 0;
			// 
			// ExamName
			// 
			this.ExamName.HeaderText = "Name";
			this.ExamName.Name = "ExamName";
			this.ExamName.ReadOnly = true;
			// 
			// Mark
			// 
			this.Mark.HeaderText = "Mark";
			this.Mark.Name = "Mark";
			this.Mark.ReadOnly = true;
			// 
			// DateTime
			// 
			this.DateTime.HeaderText = "DateTime";
			this.DateTime.Name = "DateTime";
			this.DateTime.ReadOnly = true;
			// 
			// textBoxExamName
			// 
			this.textBoxExamName.Location = new System.Drawing.Point(11, 109);
			this.textBoxExamName.Name = "textBoxExamName";
			this.textBoxExamName.Size = new System.Drawing.Size(178, 20);
			this.textBoxExamName.TabIndex = 1;
			// 
			// labelExamName
			// 
			this.labelExamName.AutoSize = true;
			this.labelExamName.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelExamName.Location = new System.Drawing.Point(11, 89);
			this.labelExamName.Name = "labelExamName";
			this.labelExamName.Size = new System.Drawing.Size(78, 17);
			this.labelExamName.TabIndex = 4;
			this.labelExamName.Text = "Exam Name";
			// 
			// labelMark
			// 
			this.labelMark.AutoSize = true;
			this.labelMark.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelMark.Location = new System.Drawing.Point(192, 89);
			this.labelMark.Name = "labelMark";
			this.labelMark.Size = new System.Drawing.Size(38, 17);
			this.labelMark.TabIndex = 5;
			this.labelMark.Text = "Mark";
			// 
			// labelDateTime
			// 
			this.labelDateTime.AutoSize = true;
			this.labelDateTime.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelDateTime.Location = new System.Drawing.Point(377, 88);
			this.labelDateTime.Name = "labelDateTime";
			this.labelDateTime.Size = new System.Drawing.Size(67, 17);
			this.labelDateTime.TabIndex = 6;
			this.labelDateTime.Text = "Date Time";
			// 
			// dateTimePicker
			// 
			this.dateTimePicker.Location = new System.Drawing.Point(380, 108);
			this.dateTimePicker.Name = "dateTimePicker";
			this.dateTimePicker.Size = new System.Drawing.Size(182, 20);
			this.dateTimePicker.TabIndex = 7;
			// 
			// buttonAdd
			// 
			this.buttonAdd.Cursor = System.Windows.Forms.Cursors.Hand;
			this.buttonAdd.Location = new System.Drawing.Point(11, 44);
			this.buttonAdd.Name = "buttonAdd";
			this.buttonAdd.Size = new System.Drawing.Size(551, 41);
			this.buttonAdd.TabIndex = 8;
			this.buttonAdd.Text = "Add";
			this.buttonAdd.UseVisualStyleBackColor = true;
			this.buttonAdd.Click += new System.EventHandler(this.Button_Add_Click);
			// 
			// comboBoxMark
			// 
			this.comboBoxMark.FormattingEnabled = true;
			this.comboBoxMark.Items.AddRange(new object[] {
            "None",
            "1",
            "2",
            "3",
            "4",
            "5"});
			this.comboBoxMark.Location = new System.Drawing.Point(195, 108);
			this.comboBoxMark.Name = "comboBoxMark";
			this.comboBoxMark.Size = new System.Drawing.Size(179, 21);
			this.comboBoxMark.TabIndex = 9;
			// 
			// labelStudentInfo
			// 
			this.labelStudentInfo.AutoSize = true;
			this.labelStudentInfo.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelStudentInfo.Location = new System.Drawing.Point(12, 20);
			this.labelStudentInfo.Name = "labelStudentInfo";
			this.labelStudentInfo.Size = new System.Drawing.Size(234, 21);
			this.labelStudentInfo.TabIndex = 10;
			this.labelStudentInfo.Text = "FirstName LastName, GroupID:";
			// 
			// ExamsForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(574, 623);
			this.Controls.Add(this.labelStudentInfo);
			this.Controls.Add(this.comboBoxMark);
			this.Controls.Add(this.buttonAdd);
			this.Controls.Add(this.dateTimePicker);
			this.Controls.Add(this.labelDateTime);
			this.Controls.Add(this.labelMark);
			this.Controls.Add(this.labelExamName);
			this.Controls.Add(this.textBoxExamName);
			this.Controls.Add(this.dataGridViewExams);
			this.MaximumSize = new System.Drawing.Size(590, 900);
			this.MinimumSize = new System.Drawing.Size(590, 300);
			this.Name = "ExamsForm";
			this.Text = "List of Exams";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ExamListForm_FormClosing);
			this.Load += new System.EventHandler(this.ExamListForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewExams)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataGridView dataGridViewExams;
		private System.Windows.Forms.DataGridViewTextBoxColumn ExamName;
		private System.Windows.Forms.DataGridViewTextBoxColumn Mark;
		private System.Windows.Forms.DataGridViewTextBoxColumn DateTime;
		private System.Windows.Forms.TextBox textBoxExamName;
		private System.Windows.Forms.Label labelExamName;
		private System.Windows.Forms.Label labelMark;
		private System.Windows.Forms.Label labelDateTime;
		private System.Windows.Forms.DateTimePicker dateTimePicker;
		private System.Windows.Forms.Button buttonAdd;
		private System.Windows.Forms.ComboBox comboBoxMark;
		private System.Windows.Forms.Label labelStudentInfo;
	}
}