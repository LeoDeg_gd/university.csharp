﻿using StudentsInformation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentsInformationForms
{
	public partial class AddStudentForm : Form
	{
		public Action<Student> OnAddStudent;
		public Action OnAddedStudent;

		public AddStudentForm ()
		{
			InitializeComponent ();
		}

		private void MainWindow_Load (object sender, EventArgs e)
		{
			comboBoxEducation.SelectedIndex = 2;
		}

		#region Events Methods

		private void Button_AddStudent_Click (object sender, EventArgs e)
		{
			if (InformationIsValid ())
			{
				OnAddStudent?.Invoke (InformationFromFormToStudent ());
				OnAddedStudent?.Invoke ();
				ClearTextBoxes ();
			}
		}

		private void ClearTextBoxes ()
		{
			textBoxFirstName.Text = string.Empty;
			textBoxLastName.Text = string.Empty;
		}

		private void TextBox_GroupID_KeyPress (object sender, KeyPressEventArgs e)
		{
			base.OnKeyPress (e);
			if (!char.IsDigit (e.KeyChar)) e.Handled = true;
			FocusToNextTextBox (e, textBoxFirstName);
		}

		private void TextBox_FirstName_KeyPress (object sender, KeyPressEventArgs e)
		{
			base.OnKeyPress (e);
			HandleDigitKeyPress (e);
			FocusToNextTextBox (e, textBoxLastName);
		}

		private void TextBox_LastName_KeyPress (object sender, KeyPressEventArgs e)
		{
			base.OnKeyPress (e);
			HandleDigitKeyPress (e);
		}

		#endregion

		private void FocusToNextTextBox (KeyPressEventArgs e, TextBox textBox)
		{
			if ((Keys)e.KeyChar == Keys.Enter)
				textBox.Focus ();
		}

		private static void HandleDigitKeyPress (KeyPressEventArgs e)
		{
			if (char.IsDigit (e.KeyChar))
				e.Handled = true;
		}

		private Student InformationFromFormToStudent ()
		{
			return new Student (
					int.Parse (textBoxGroupID.Text),
					textBoxFirstName.Text,
					textBoxLastName.Text,
					dateTimePicker.Value,
					(Education)comboBoxEducation.SelectedIndex);
		}

		private bool InformationIsValid ()
		{
			labelGroupID.ForeColor = textBoxGroupID.Text.Length == 0 ? Color.Red : Color.Black;
			labelFirstName.ForeColor = textBoxFirstName.Text.Length <= 1 ? Color.Red : Color.Black;
			labelLastName.ForeColor = textBoxLastName.Text.Length <= 1 ? Color.Red : Color.Black;

			return textBoxGroupID.Text.Length > 0 &&
				textBoxFirstName.Text.Length > 1 &&
				textBoxLastName.Text.Length > 1;
		}
	}
}
