﻿namespace StudentsInformationForms
{
	partial class AddStudentForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose ();
			}
			base.Dispose (disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
			this.labelTitle = new System.Windows.Forms.Label();
			this.labelGroupID = new System.Windows.Forms.Label();
			this.textBoxGroupID = new System.Windows.Forms.TextBox();
			this.textBoxFirstName = new System.Windows.Forms.TextBox();
			this.labelFirstName = new System.Windows.Forms.Label();
			this.textBoxLastName = new System.Windows.Forms.TextBox();
			this.labelLastName = new System.Windows.Forms.Label();
			this.labelDateOfBirth = new System.Windows.Forms.Label();
			this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
			this.labelEducation = new System.Windows.Forms.Label();
			this.comboBoxEducation = new System.Windows.Forms.ComboBox();
			this.buttonSave = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// labelTitle
			// 
			this.labelTitle.AutoSize = true;
			this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelTitle.Location = new System.Drawing.Point(37, 24);
			this.labelTitle.Name = "labelTitle";
			this.labelTitle.Size = new System.Drawing.Size(200, 25);
			this.labelTitle.TabIndex = 0;
			this.labelTitle.Text = "Student Information";
			// 
			// labelGroupID
			// 
			this.labelGroupID.AutoSize = true;
			this.labelGroupID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelGroupID.Location = new System.Drawing.Point(37, 63);
			this.labelGroupID.Name = "labelGroupID";
			this.labelGroupID.Size = new System.Drawing.Size(84, 17);
			this.labelGroupID.TabIndex = 1;
			this.labelGroupID.Text = "Group ID *";
			// 
			// textBoxGroupID
			// 
			this.textBoxGroupID.Location = new System.Drawing.Point(42, 83);
			this.textBoxGroupID.Name = "textBoxGroupID";
			this.textBoxGroupID.Size = new System.Drawing.Size(195, 20);
			this.textBoxGroupID.TabIndex = 2;
			this.textBoxGroupID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_GroupID_KeyPress);
			// 
			// textBoxFirstName
			// 
			this.textBoxFirstName.Location = new System.Drawing.Point(42, 132);
			this.textBoxFirstName.Name = "textBoxFirstName";
			this.textBoxFirstName.Size = new System.Drawing.Size(195, 20);
			this.textBoxFirstName.TabIndex = 4;
			this.textBoxFirstName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_FirstName_KeyPress);
			// 
			// labelFirstName
			// 
			this.labelFirstName.AutoSize = true;
			this.labelFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelFirstName.Location = new System.Drawing.Point(37, 112);
			this.labelFirstName.Name = "labelFirstName";
			this.labelFirstName.Size = new System.Drawing.Size(97, 17);
			this.labelFirstName.TabIndex = 3;
			this.labelFirstName.Text = "First Name *";
			// 
			// textBoxLastName
			// 
			this.textBoxLastName.Location = new System.Drawing.Point(42, 179);
			this.textBoxLastName.Name = "textBoxLastName";
			this.textBoxLastName.Size = new System.Drawing.Size(195, 20);
			this.textBoxLastName.TabIndex = 6;
			this.textBoxLastName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_LastName_KeyPress);
			// 
			// labelLastName
			// 
			this.labelLastName.AutoSize = true;
			this.labelLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelLastName.Location = new System.Drawing.Point(37, 159);
			this.labelLastName.Name = "labelLastName";
			this.labelLastName.Size = new System.Drawing.Size(96, 17);
			this.labelLastName.TabIndex = 5;
			this.labelLastName.Text = "Last Name *";
			// 
			// labelDateOfBirth
			// 
			this.labelDateOfBirth.AutoSize = true;
			this.labelDateOfBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelDateOfBirth.Location = new System.Drawing.Point(37, 205);
			this.labelDateOfBirth.Name = "labelDateOfBirth";
			this.labelDateOfBirth.Size = new System.Drawing.Size(100, 17);
			this.labelDateOfBirth.TabIndex = 7;
			this.labelDateOfBirth.Text = "Date of Birth";
			// 
			// dateTimePicker
			// 
			this.dateTimePicker.Location = new System.Drawing.Point(42, 225);
			this.dateTimePicker.Name = "dateTimePicker";
			this.dateTimePicker.Size = new System.Drawing.Size(195, 20);
			this.dateTimePicker.TabIndex = 8;
			this.dateTimePicker.Value = new System.DateTime(1997, 1, 1, 0, 0, 0, 0);
			// 
			// labelEducation
			// 
			this.labelEducation.AutoSize = true;
			this.labelEducation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelEducation.Location = new System.Drawing.Point(37, 255);
			this.labelEducation.Name = "labelEducation";
			this.labelEducation.Size = new System.Drawing.Size(80, 17);
			this.labelEducation.TabIndex = 9;
			this.labelEducation.Text = "Education";
			// 
			// comboBoxEducation
			// 
			this.comboBoxEducation.DisplayMember = "Bachelor";
			this.comboBoxEducation.FormattingEnabled = true;
			this.comboBoxEducation.Items.AddRange(new object[] {
            "None",
            "Specialist",
            "Bachelor",
            "SecondEducation"});
			this.comboBoxEducation.Location = new System.Drawing.Point(42, 275);
			this.comboBoxEducation.Name = "comboBoxEducation";
			this.comboBoxEducation.Size = new System.Drawing.Size(195, 21);
			this.comboBoxEducation.TabIndex = 10;
			// 
			// buttonSave
			// 
			this.buttonSave.BackColor = System.Drawing.SystemColors.ControlLight;
			this.buttonSave.Cursor = System.Windows.Forms.Cursors.Hand;
			this.buttonSave.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonSave.Location = new System.Drawing.Point(42, 320);
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.Size = new System.Drawing.Size(195, 60);
			this.buttonSave.TabIndex = 11;
			this.buttonSave.Text = "Add Student";
			this.buttonSave.UseVisualStyleBackColor = true;
			this.buttonSave.Click += new System.EventHandler(this.Button_AddStudent_Click);
			// 
			// AddStudentForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(274, 401);
			this.Controls.Add(this.buttonSave);
			this.Controls.Add(this.comboBoxEducation);
			this.Controls.Add(this.labelEducation);
			this.Controls.Add(this.dateTimePicker);
			this.Controls.Add(this.labelDateOfBirth);
			this.Controls.Add(this.textBoxLastName);
			this.Controls.Add(this.labelLastName);
			this.Controls.Add(this.textBoxFirstName);
			this.Controls.Add(this.labelFirstName);
			this.Controls.Add(this.textBoxGroupID);
			this.Controls.Add(this.labelGroupID);
			this.Controls.Add(this.labelTitle);
			this.MaximumSize = new System.Drawing.Size(290, 440);
			this.MinimumSize = new System.Drawing.Size(290, 440);
			this.Name = "AddStudentForm";
			this.ShowIcon = false;
			this.Text = "Student Info";
			this.Load += new System.EventHandler(this.MainWindow_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label labelTitle;
		private System.Windows.Forms.Label labelGroupID;
		private System.Windows.Forms.TextBox textBoxGroupID;
		private System.Windows.Forms.TextBox textBoxFirstName;
		private System.Windows.Forms.Label labelFirstName;
		private System.Windows.Forms.TextBox textBoxLastName;
		private System.Windows.Forms.Label labelLastName;
		private System.Windows.Forms.Label labelDateOfBirth;
		private System.Windows.Forms.DateTimePicker dateTimePicker;
		private System.Windows.Forms.Label labelEducation;
		private System.Windows.Forms.ComboBox comboBoxEducation;
		private System.Windows.Forms.Button buttonSave;
	}
}

