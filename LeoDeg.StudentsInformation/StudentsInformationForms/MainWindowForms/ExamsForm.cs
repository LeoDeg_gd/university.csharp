﻿using StudentsInformation;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace StudentsInformationForms
{
	public partial class ExamsForm : Form
	{
		public List<Exam> Exams { get; set; }
		public Action OnSaveInfo;

		public ExamsForm ()
		{
			InitializeComponent ();
		}

		private void ExamListForm_Load (object sender, EventArgs e)
		{
			comboBoxMark.SelectedIndex = 0;
			DisplayExamsToGridView (Exams);
		}

		private void ExamListForm_FormClosing (object sender, FormClosingEventArgs e)
		{
			Exams = new List<Exam> ();
		}

		private void Button_Add_Click (object sender, EventArgs e)
		{
			if (InformationIsValid ())
			{
				Exams.Add (new Exam (textBoxExamName.Text, comboBoxMark.SelectedIndex, dateTimePicker.Value));
				DisplayExamsToGridView (Exams);
			}
		}

		private bool InformationIsValid ()
		{
			labelExamName.ForeColor = textBoxExamName.Text.Length == 0 ? Color.Red : Color.Black;
			labelMark.ForeColor = comboBoxMark.SelectedIndex == 0 ? Color.Red : Color.Black;

			return textBoxExamName.Text.Length > 0 &&
				comboBoxMark.SelectedIndex > 0;
		}

		private void DisplayExamsToGridView (List<Exam> exams)
		{
			if (exams.Count > 0)
			{
				int rowIndex = 0;
				foreach (Exam exam in exams)
				{
					if (dataGridViewExams.Rows.Count < exams.Count)
						dataGridViewExams.Rows.Add ();
					DisplayExamToGridView (rowIndex, exam);
					++rowIndex;
				}
			}
		}

		private void DisplayExamToGridView (int rowIndex, Exam exam)
		{
			dataGridViewExams.Rows[rowIndex].Cells[0].Value = exam.Name;
			dataGridViewExams.Rows[rowIndex].Cells[1].Value = exam.Mark;
			dataGridViewExams.Rows[rowIndex].Cells[2].Value = exam.DateTime.ToShortDateString ();
		}

		public void DisplayStudentInfo (string firstName, string lastName, int groupID)
		{
			labelStudentInfo.Text = string.Format("{0}, {1}, groupID: {2}", firstName, lastName, groupID);
		}
	}
}
