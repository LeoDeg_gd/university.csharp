﻿using StudentsInformation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentsInformationForms
{
	public partial class SortInformationForm : Form
	{
		public Action OnSort;
		public Action<StudentsCollection.SortType> OnSortByFirstName;
		public Action<StudentsCollection.SortType> OnSortByLastName;
		public Action<StudentsCollection.SortType> OnSortByDateOfBirth;
		public Action<StudentsCollection.SortType> OnSortByExamsAverageRating;

		public SortInformationForm ()
		{
			InitializeComponent ();
		}

		private void Button_SortByFirstName_Click (object sender, EventArgs e)
		{
			OnSortByFirstName?.Invoke (StudentsCollection.SortType.FirtName);
			OnSort?.Invoke ();
		}

		private void Button_SortByLastName_Click (object sender, EventArgs e)
		{
			OnSortByLastName?.Invoke (StudentsCollection.SortType.LastName);
			OnSort?.Invoke ();
		}

		private void Button_SortByDateOfBirth_Click (object sender, EventArgs e)
		{
			OnSortByDateOfBirth?.Invoke (StudentsCollection.SortType.DateOfBirth);
			OnSort?.Invoke ();
		}

		private void Button_SortByExamsAverageRating_Click (object sender, EventArgs e)
		{
			OnSortByExamsAverageRating?.Invoke (StudentsCollection.SortType.AverageExamsRating);
			OnSort?.Invoke ();
		}
	}
}
