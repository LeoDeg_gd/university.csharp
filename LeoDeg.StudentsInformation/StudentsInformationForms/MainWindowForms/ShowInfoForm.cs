﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentsInformationForms
{
	public partial class ShowInfoForm : Form
	{
		public Func<string> OnShowInfo;

		public ShowInfoForm ()
		{
			InitializeComponent ();
		}

		private void ShowInfo_Load (object sender, EventArgs e)
		{
			if (OnShowInfo != null)
				richTextBox.Text = OnShowInfo.Invoke ();
		}
	}
}
