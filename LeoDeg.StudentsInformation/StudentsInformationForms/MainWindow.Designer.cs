﻿namespace StudentsInformationForms
{
	partial class MainWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
			this.dataGridViewStudentsInfo = new System.Windows.Forms.DataGridView();
			this.GroupID = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.LastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.DateOfBirth = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Education = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Exams = new System.Windows.Forms.DataGridViewButtonColumn();
			this.Tests = new System.Windows.Forms.DataGridViewButtonColumn();
			this.Delete = new System.Windows.Forms.DataGridViewButtonColumn();
			this.printDocument = new System.Drawing.Printing.PrintDocument();
			this.printPreviewDialog = new System.Windows.Forms.PrintPreviewDialog();
			this.labelStudentsAmount = new System.Windows.Forms.Label();
			this.menuStrip = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.reloadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openLogsDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openInformationFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.showLogsInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.showStudentsInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.addStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.updateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.sortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.printToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
			this.buttonAddStudent = new System.Windows.Forms.Button();
			this.buttonUpdate = new System.Windows.Forms.Button();
			this.buttonSave = new System.Windows.Forms.Button();
			this.buttonReload = new System.Windows.Forms.Button();
			this.buttonPrint = new System.Windows.Forms.Button();
			this.buttonOpenFile = new System.Windows.Forms.Button();
			this.buttonSort = new System.Windows.Forms.Button();
			this.buttonOpenLogs = new System.Windows.Forms.Button();
			this.buttonShowStudentsInfo = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudentsInfo)).BeginInit();
			this.menuStrip.SuspendLayout();
			this.contextMenuStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// dataGridViewStudentsInfo
			// 
			this.dataGridViewStudentsInfo.AllowUserToAddRows = false;
			this.dataGridViewStudentsInfo.AllowUserToDeleteRows = false;
			this.dataGridViewStudentsInfo.AllowUserToResizeRows = false;
			this.dataGridViewStudentsInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dataGridViewStudentsInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dataGridViewStudentsInfo.BackgroundColor = System.Drawing.Color.White;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.SkyBlue;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
			dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewStudentsInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dataGridViewStudentsInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewStudentsInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GroupID,
            this.FirstName,
            this.LastName,
            this.DateOfBirth,
            this.Education,
            this.Exams,
            this.Tests,
            this.Delete});
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
			dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridViewStudentsInfo.DefaultCellStyle = dataGridViewCellStyle2;
			this.dataGridViewStudentsInfo.GridColor = System.Drawing.SystemColors.Control;
			this.dataGridViewStudentsInfo.Location = new System.Drawing.Point(12, 65);
			this.dataGridViewStudentsInfo.Name = "dataGridViewStudentsInfo";
			this.dataGridViewStudentsInfo.ReadOnly = true;
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 9F);
			dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewStudentsInfo.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.dataGridViewStudentsInfo.RowHeadersVisible = false;
			this.dataGridViewStudentsInfo.Size = new System.Drawing.Size(810, 591);
			this.dataGridViewStudentsInfo.StandardTab = true;
			this.dataGridViewStudentsInfo.TabIndex = 0;
			this.dataGridViewStudentsInfo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewStudentsInfo_CellContentClick);
			this.dataGridViewStudentsInfo.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridViewStudentsInfo_MouseClick);
			// 
			// GroupID
			// 
			this.GroupID.FillWeight = 119.5594F;
			this.GroupID.HeaderText = "GroupID";
			this.GroupID.Name = "GroupID";
			this.GroupID.ReadOnly = true;
			// 
			// FirstName
			// 
			this.FirstName.FillWeight = 119.5594F;
			this.FirstName.HeaderText = "FirstName";
			this.FirstName.Name = "FirstName";
			this.FirstName.ReadOnly = true;
			// 
			// LastName
			// 
			this.LastName.FillWeight = 119.5594F;
			this.LastName.HeaderText = "LastName";
			this.LastName.Name = "LastName";
			this.LastName.ReadOnly = true;
			// 
			// DateOfBirth
			// 
			this.DateOfBirth.FillWeight = 119.5594F;
			this.DateOfBirth.HeaderText = "Date Of Birth";
			this.DateOfBirth.Name = "DateOfBirth";
			this.DateOfBirth.ReadOnly = true;
			// 
			// Education
			// 
			this.Education.FillWeight = 119.5594F;
			this.Education.HeaderText = "Education";
			this.Education.Name = "Education";
			this.Education.ReadOnly = true;
			// 
			// Exams
			// 
			this.Exams.FillWeight = 70F;
			this.Exams.HeaderText = "Exams";
			this.Exams.Name = "Exams";
			this.Exams.ReadOnly = true;
			this.Exams.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.Exams.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			// 
			// Tests
			// 
			this.Tests.FillWeight = 70F;
			this.Tests.HeaderText = "Tests";
			this.Tests.Name = "Tests";
			this.Tests.ReadOnly = true;
			this.Tests.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.Tests.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			// 
			// Delete
			// 
			this.Delete.FillWeight = 70F;
			this.Delete.HeaderText = "Delete";
			this.Delete.Name = "Delete";
			this.Delete.ReadOnly = true;
			this.Delete.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.Delete.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			// 
			// printDocument
			// 
			this.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument_PrintPage);
			// 
			// printPreviewDialog
			// 
			this.printPreviewDialog.AutoScrollMargin = new System.Drawing.Size(0, 0);
			this.printPreviewDialog.AutoScrollMinSize = new System.Drawing.Size(0, 0);
			this.printPreviewDialog.ClientSize = new System.Drawing.Size(400, 300);
			this.printPreviewDialog.Enabled = true;
			this.printPreviewDialog.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog.Icon")));
			this.printPreviewDialog.Name = "printPreviewDialog1";
			this.printPreviewDialog.Visible = false;
			// 
			// labelStudentsAmount
			// 
			this.labelStudentsAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.labelStudentsAmount.Location = new System.Drawing.Point(12, 659);
			this.labelStudentsAmount.Name = "labelStudentsAmount";
			this.labelStudentsAmount.Size = new System.Drawing.Size(369, 14);
			this.labelStudentsAmount.TabIndex = 4;
			this.labelStudentsAmount.Text = "Amount";
			// 
			// menuStrip
			// 
			this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem});
			this.menuStrip.Location = new System.Drawing.Point(0, 0);
			this.menuStrip.Name = "menuStrip";
			this.menuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
			this.menuStrip.Size = new System.Drawing.Size(834, 24);
			this.menuStrip.TabIndex = 12;
			this.menuStrip.Text = "Menu";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.reloadToolStripMenuItem,
            this.openLogsDirectoryToolStripMenuItem,
            this.openInformationFileToolStripMenuItem,
            this.showLogsInfoToolStripMenuItem,
            this.showStudentsInfoToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.saveToolStripMenuItem.Text = "Save";
			this.saveToolStripMenuItem.Click += new System.EventHandler(this.Save_ToolStripMenuItem_Click);
			// 
			// reloadToolStripMenuItem
			// 
			this.reloadToolStripMenuItem.Name = "reloadToolStripMenuItem";
			this.reloadToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.reloadToolStripMenuItem.Text = "Reload";
			this.reloadToolStripMenuItem.Click += new System.EventHandler(this.Reload_ToolStripMenuItem_Click);
			// 
			// openLogsDirectoryToolStripMenuItem
			// 
			this.openLogsDirectoryToolStripMenuItem.Name = "openLogsDirectoryToolStripMenuItem";
			this.openLogsDirectoryToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.openLogsDirectoryToolStripMenuItem.Text = "Open Logs";
			this.openLogsDirectoryToolStripMenuItem.Click += new System.EventHandler(this.OpenLogsDirectory_ToolStripMenuItem_Click);
			// 
			// openInformationFileToolStripMenuItem
			// 
			this.openInformationFileToolStripMenuItem.Name = "openInformationFileToolStripMenuItem";
			this.openInformationFileToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.openInformationFileToolStripMenuItem.Text = "Open XML File";
			this.openInformationFileToolStripMenuItem.Click += new System.EventHandler(this.OpenInformationFile_ToolStripMenuItem_Click);
			// 
			// showLogsInfoToolStripMenuItem
			// 
			this.showLogsInfoToolStripMenuItem.Name = "showLogsInfoToolStripMenuItem";
			this.showLogsInfoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.showLogsInfoToolStripMenuItem.Text = "Show Logs Info";
			this.showLogsInfoToolStripMenuItem.Click += new System.EventHandler(this.ShowLogsInfo_ToolStripMenuItem_Click);
			// 
			// showStudentsInfoToolStripMenuItem
			// 
			this.showStudentsInfoToolStripMenuItem.Name = "showStudentsInfoToolStripMenuItem";
			this.showStudentsInfoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.showStudentsInfoToolStripMenuItem.Text = "Show Students Info";
			this.showStudentsInfoToolStripMenuItem.Click += new System.EventHandler(this.ShowStudentsInfo_ToolStripMenuItem_Click);
			// 
			// toolsToolStripMenuItem
			// 
			this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addStudentToolStripMenuItem,
            this.updateToolStripMenuItem,
            this.sortToolStripMenuItem,
            this.printToolStripMenuItem1});
			this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
			this.toolsToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
			this.toolsToolStripMenuItem.Text = "Information";
			// 
			// addStudentToolStripMenuItem
			// 
			this.addStudentToolStripMenuItem.Name = "addStudentToolStripMenuItem";
			this.addStudentToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.addStudentToolStripMenuItem.Text = "Add Student";
			this.addStudentToolStripMenuItem.Click += new System.EventHandler(this.AddStudent_ToolStripMenuItem_Click);
			// 
			// updateToolStripMenuItem
			// 
			this.updateToolStripMenuItem.Name = "updateToolStripMenuItem";
			this.updateToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.updateToolStripMenuItem.Text = "Update Table";
			this.updateToolStripMenuItem.Click += new System.EventHandler(this.Update_ToolStripMenuItem_Click);
			// 
			// sortToolStripMenuItem
			// 
			this.sortToolStripMenuItem.Name = "sortToolStripMenuItem";
			this.sortToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.sortToolStripMenuItem.Text = "Sort Table";
			this.sortToolStripMenuItem.Click += new System.EventHandler(this.Sort_ToolStripMenuItem_Click);
			// 
			// printToolStripMenuItem1
			// 
			this.printToolStripMenuItem1.Name = "printToolStripMenuItem1";
			this.printToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
			this.printToolStripMenuItem1.Text = "Print";
			// 
			// contextMenuStrip
			// 
			this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4,
            this.toolStripMenuItem1,
            this.toolStripMenuItem3,
            this.toolStripMenuItem2});
			this.contextMenuStrip.Name = "contextMenuStrip";
			this.contextMenuStrip.Size = new System.Drawing.Size(113, 92);
			// 
			// toolStripMenuItem4
			// 
			this.toolStripMenuItem4.Name = "toolStripMenuItem4";
			this.toolStripMenuItem4.Size = new System.Drawing.Size(112, 22);
			this.toolStripMenuItem4.Text = "Sort";
			this.toolStripMenuItem4.Click += new System.EventHandler(this.ToolStripMenuItem_SortTable_Click);
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(112, 22);
			this.toolStripMenuItem1.Text = "Save";
			this.toolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItem_Save_Click);
			// 
			// toolStripMenuItem3
			// 
			this.toolStripMenuItem3.Name = "toolStripMenuItem3";
			this.toolStripMenuItem3.Size = new System.Drawing.Size(112, 22);
			this.toolStripMenuItem3.Text = "Update";
			this.toolStripMenuItem3.Click += new System.EventHandler(this.ToolStripMenuItem_UpdateTable_Click);
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(112, 22);
			this.toolStripMenuItem2.Text = "Reload";
			this.toolStripMenuItem2.Click += new System.EventHandler(this.ToolStripMenuItem_Reload_Click);
			// 
			// buttonAddStudent
			// 
			this.buttonAddStudent.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonAddStudent.Location = new System.Drawing.Point(12, 36);
			this.buttonAddStudent.Name = "buttonAddStudent";
			this.buttonAddStudent.Size = new System.Drawing.Size(100, 23);
			this.buttonAddStudent.TabIndex = 13;
			this.buttonAddStudent.Text = "Add Student";
			this.buttonAddStudent.UseVisualStyleBackColor = true;
			this.buttonAddStudent.Click += new System.EventHandler(this.Button_AddStudent_Click);
			// 
			// buttonUpdate
			// 
			this.buttonUpdate.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonUpdate.Location = new System.Drawing.Point(118, 36);
			this.buttonUpdate.Name = "buttonUpdate";
			this.buttonUpdate.Size = new System.Drawing.Size(73, 23);
			this.buttonUpdate.TabIndex = 14;
			this.buttonUpdate.Text = "Update";
			this.buttonUpdate.UseVisualStyleBackColor = true;
			this.buttonUpdate.Click += new System.EventHandler(this.Button_Update_Click);
			// 
			// buttonSave
			// 
			this.buttonSave.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonSave.Location = new System.Drawing.Point(197, 36);
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.Size = new System.Drawing.Size(73, 23);
			this.buttonSave.TabIndex = 15;
			this.buttonSave.Text = "Save";
			this.buttonSave.UseVisualStyleBackColor = true;
			this.buttonSave.Click += new System.EventHandler(this.Button_Save_Click);
			// 
			// buttonReload
			// 
			this.buttonReload.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonReload.Location = new System.Drawing.Point(276, 36);
			this.buttonReload.Name = "buttonReload";
			this.buttonReload.Size = new System.Drawing.Size(73, 23);
			this.buttonReload.TabIndex = 16;
			this.buttonReload.Text = "Reload";
			this.buttonReload.UseVisualStyleBackColor = true;
			this.buttonReload.Click += new System.EventHandler(this.Button_Reload_Click);
			// 
			// buttonPrint
			// 
			this.buttonPrint.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonPrint.Location = new System.Drawing.Point(749, 36);
			this.buttonPrint.Name = "buttonPrint";
			this.buttonPrint.Size = new System.Drawing.Size(73, 23);
			this.buttonPrint.TabIndex = 17;
			this.buttonPrint.Text = "Print";
			this.buttonPrint.UseVisualStyleBackColor = true;
			this.buttonPrint.Click += new System.EventHandler(this.Button_Print_Click);
			// 
			// buttonOpenFile
			// 
			this.buttonOpenFile.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonOpenFile.Location = new System.Drawing.Point(670, 36);
			this.buttonOpenFile.Name = "buttonOpenFile";
			this.buttonOpenFile.Size = new System.Drawing.Size(73, 23);
			this.buttonOpenFile.TabIndex = 18;
			this.buttonOpenFile.Text = "Open File";
			this.buttonOpenFile.UseVisualStyleBackColor = true;
			this.buttonOpenFile.Click += new System.EventHandler(this.Button_OpenFile_Click);
			// 
			// buttonSort
			// 
			this.buttonSort.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonSort.Location = new System.Drawing.Point(512, 36);
			this.buttonSort.Name = "buttonSort";
			this.buttonSort.Size = new System.Drawing.Size(73, 23);
			this.buttonSort.TabIndex = 19;
			this.buttonSort.Text = "Sort";
			this.buttonSort.UseVisualStyleBackColor = true;
			this.buttonSort.Click += new System.EventHandler(this.Button_Sort_Click);
			// 
			// buttonOpenLogs
			// 
			this.buttonOpenLogs.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonOpenLogs.Location = new System.Drawing.Point(591, 36);
			this.buttonOpenLogs.Name = "buttonOpenLogs";
			this.buttonOpenLogs.Size = new System.Drawing.Size(73, 23);
			this.buttonOpenLogs.TabIndex = 20;
			this.buttonOpenLogs.Text = "Open Logs";
			this.buttonOpenLogs.UseVisualStyleBackColor = true;
			this.buttonOpenLogs.Click += new System.EventHandler(this.Button_OpenLogs_Click);
			// 
			// buttonShowStudentsInfo
			// 
			this.buttonShowStudentsInfo.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonShowStudentsInfo.Location = new System.Drawing.Point(355, 36);
			this.buttonShowStudentsInfo.Name = "buttonShowStudentsInfo";
			this.buttonShowStudentsInfo.Size = new System.Drawing.Size(151, 23);
			this.buttonShowStudentsInfo.TabIndex = 22;
			this.buttonShowStudentsInfo.Text = "Show Students Info";
			this.buttonShowStudentsInfo.UseVisualStyleBackColor = true;
			this.buttonShowStudentsInfo.Click += new System.EventHandler(this.Button_ShowStudentsInfo_Click);
			// 
			// MainWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(834, 681);
			this.Controls.Add(this.buttonShowStudentsInfo);
			this.Controls.Add(this.buttonOpenLogs);
			this.Controls.Add(this.buttonSort);
			this.Controls.Add(this.buttonOpenFile);
			this.Controls.Add(this.buttonPrint);
			this.Controls.Add(this.buttonReload);
			this.Controls.Add(this.buttonSave);
			this.Controls.Add(this.buttonUpdate);
			this.Controls.Add(this.buttonAddStudent);
			this.Controls.Add(this.menuStrip);
			this.Controls.Add(this.labelStudentsAmount);
			this.Controls.Add(this.dataGridViewStudentsInfo);
			this.MainMenuStrip = this.menuStrip;
			this.MaximumSize = new System.Drawing.Size(850, 720);
			this.MinimumSize = new System.Drawing.Size(850, 720);
			this.Name = "MainWindow";
			this.ShowIcon = false;
			this.Text = "Students Information";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StudentsInformationGrid_FormClosing);
			this.Load += new System.EventHandler(this.StudentsInformationGrid_Load);
			this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MainWindow_MouseClick);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudentsInfo)).EndInit();
			this.menuStrip.ResumeLayout(false);
			this.menuStrip.PerformLayout();
			this.contextMenuStrip.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataGridView dataGridViewStudentsInfo;
		private System.Drawing.Printing.PrintDocument printDocument;
		private System.Windows.Forms.PrintPreviewDialog printPreviewDialog;
		private System.Windows.Forms.Label labelStudentsAmount;
		private System.Windows.Forms.DataGridViewTextBoxColumn GroupID;
		private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
		private System.Windows.Forms.DataGridViewTextBoxColumn LastName;
		private System.Windows.Forms.DataGridViewTextBoxColumn DateOfBirth;
		private System.Windows.Forms.DataGridViewTextBoxColumn Education;
		private System.Windows.Forms.DataGridViewButtonColumn Exams;
		private System.Windows.Forms.DataGridViewButtonColumn Tests;
		private System.Windows.Forms.DataGridViewButtonColumn Delete;
		private System.Windows.Forms.MenuStrip menuStrip;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem reloadToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openLogsDirectoryToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openInformationFileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem addStudentToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem updateToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem sortToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem showStudentsInfoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem showLogsInfoToolStripMenuItem;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
		private System.Windows.Forms.Button buttonAddStudent;
		private System.Windows.Forms.Button buttonUpdate;
		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Button buttonReload;
		private System.Windows.Forms.Button buttonPrint;
		private System.Windows.Forms.Button buttonOpenFile;
		private System.Windows.Forms.Button buttonSort;
		private System.Windows.Forms.Button buttonOpenLogs;
		private System.Windows.Forms.Button buttonShowStudentsInfo;
	}
}