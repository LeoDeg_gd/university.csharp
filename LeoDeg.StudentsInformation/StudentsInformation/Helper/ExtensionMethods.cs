﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace StudentsInformation
{
	public static class ExtensionMethods
	{
		public static int AttributeAsInt (this XmlNode node, string attributeName)
		{
			return Convert.ToInt32 (node.AttributeAsString (attributeName));
		}

		public static DateTime AttributeAsDateTime (this XmlNode node, string attributeName)
		{
			return Convert.ToDateTime (node.AttributeAsString (attributeName));
		}

		public static Education AttributeAsEducation (this XmlNode node, string attributeName)
		{
			switch (node.AttributeAsString (attributeName))
			{
				case "None": return Education.None;
				case "Specialist": return Education.Specialist;
				case "Bachelor": return Education.Bachelor;
				case "SecondEducation": return Education.SecondEducation;
				default: throw new ArgumentException ($"Education enum does not contain: {attributeName}.");
			}
		}

		public static string AttributeAsString (this XmlNode node, string attributeName)
		{
			XmlAttribute attribute = node.Attributes?[attributeName];
			if (attribute == null)
				throw new ArgumentException ($"The attribute '{attributeName}' does not exist");
			return attribute.Value;
		}

		/// <summary>
		/// Return list of person info, group id and education information.
		/// </summary>
		public static string ToShortString (this List<Student> students)
		{
			StringBuilder builder = new StringBuilder ();

			foreach (Student student in students)
			{
				builder.Append (student.ToShortString ());
			}

			return builder.ToString ();
		}
	}
}
