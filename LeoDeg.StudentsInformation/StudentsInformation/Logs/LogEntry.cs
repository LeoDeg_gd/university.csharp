﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsInformation
{
	public class LogEntry
	{
		public const string Default = "None";

		public string CollectionName { get; set; }
		public string LogInfo { get; set; }
		public string StudentInfo { get; set; }
		public string ChangesType { get; set; }
		public DateTime Time { get; set; }


		public LogEntry (string collectionName, string info, string studentInfo, string changesType, DateTime time)
		{
			CollectionName = collectionName;
			LogInfo = info;
			StudentInfo = studentInfo;
			ChangesType = changesType;
			Time = time;
		}

		public override string ToString ()
		{
			return string.Format ("Collection name: {0} \nChanges name{1} \nInfo: {2} \nStudent info: {3} \nLog time: {4}", CollectionName, ChangesType, LogInfo, StudentInfo, Time.ToString ("s"));
		}
	}
}
