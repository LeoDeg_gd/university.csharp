﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsInformation
{
	public class Logs
	{
		public List<LogEntry> JournalEntries { get; set; }

		private readonly LogsWriter journalWriter;

		public Logs ()
		{
			journalWriter = new LogsWriter ();
			JournalEntries = new List<LogEntry> ();
		}

		public void AddEntry (string collectionName, string info, string studentinfo, string changesType, DateTime time)
		{
			AddEntry (new LogEntry (collectionName, info, studentinfo, changesType, time));
		}

		public void AddEntry (LogEntry entry)
		{
			JournalEntries.Add (entry);
		}

		public void CollectionChanged (object sender, StudentEventArgs e)
		{
			JournalEntries.Add (new LogEntry (
				"StudentsCollection",
				e.LogInfo ?? LogEntry.Default,
				e.StudentInfo ?? LogEntry.Default,
				e.Action.ToString () ?? LogEntry.Default,
				e.EventTime)
			);
		}

		public void SaveToFileAndClear ()
		{
			journalWriter.SaveToFile (ToString);
			JournalEntries.Clear ();
		}

		/// <summary>
		/// Return string of journal entries information.
		/// </summary>
		public override string ToString ()
		{
			lock (JournalEntries)
			{
				StringBuilder builder = new StringBuilder ();

				foreach (LogEntry entry in JournalEntries)
				{
					builder.Append (entry.ToString ());
					builder.Append ("\n\n");
					builder.Append ("=================================================");
					builder.Append ("\n\n");
				}

				return builder.ToString ();
			}
		}
	}
}
