﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsInformation
{
	public class LogsWriter
	{
		public LogsWriter ()
		{
			if (!Directory.Exists ("Logs"))
				Directory.CreateDirectory ("Logs");
		}

		public void SaveToFile (Func<string> toString)
		{
			string fileName = string.Format ("Logs\\Log_{0}.txt", DateTime.Now.ToString ("s").Replace (":", "_").Replace ("-", "_"));

			using (StreamWriter stream = File.CreateText (fileName))
			{
				stream.Write (toString.Invoke ());
			}
		}
	}
}
