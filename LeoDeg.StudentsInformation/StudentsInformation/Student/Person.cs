﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsInformation
{
	public class Person : ICloneable, IComparable, IComparer<Person>
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public DateTime DateOfBirth { get; set; }

		public Person () : this (string.Empty, string.Empty, DateTime.MinValue) { }

		public Person (string firstName, string lastName, DateTime dateOfBirth)
		{
			FirstName = firstName;
			LastName = lastName;
			DateOfBirth = dateOfBirth;
		}

		/// <summary>
		/// Return first name, last name and data of birth.
		/// </summary>
		public override string ToString ()
		{
			return string.Format ("First Name: {0}, Last Name: {1}, Date of Birth: {2}", FirstName, LastName, DateOfBirth.ToString ());
		}

		/// <summary>
		/// Return first name and last name.
		/// </summary>
		public virtual string ToShortString ()
		{
			return string.Format ("First Name: {0}, Last Name: {1}", FirstName, LastName);
		}

		/// <summary>
		/// Compare fields with fields of object.
		/// </summary>
		public override bool Equals (object obj)
		{
			if (!(obj is Person other))
				return false;

			return FirstName == other.FirstName &&
				LastName == other.LastName &&
				DateOfBirth == other.DateOfBirth;
		}

		public virtual object Clone ()
		{
			return new Person (FirstName, LastName, DateOfBirth) as object;
		}

		public override int GetHashCode ()
		{
			return base.GetHashCode ();
		}

		/// <summary>
		/// Compare by person' last name.
		/// </summary>
		public int CompareTo (Person other)
		{
			return LastName.CompareTo (other.LastName);
		}

		/// <summary>
		/// Compare by person' last name.
		/// </summary>
		public int CompareTo (object obj)
		{
			Person person = obj as Person;
			if (person == null)
				throw new ArgumentNullException ();

			return LastName.CompareTo (person.LastName);
		}

		/// <summary>
		/// Compare by person' date of birth.
		/// </summary>
		public int Compare (Person x, Person y)
		{
			if (x.DateOfBirth < y.DateOfBirth) return -1;
			if (x.DateOfBirth > y.DateOfBirth) return 1;
			return 0;
		}

		public static bool operator == (Person source, Person other)
		{
			return source.Equals (other);
		}

		public static bool operator != (Person source, Person other)
		{
			return !source.Equals (other);
		}
	}
}
