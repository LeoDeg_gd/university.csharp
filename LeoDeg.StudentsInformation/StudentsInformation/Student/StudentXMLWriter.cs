﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace StudentsInformation
{
	public class StudentXMLWriter
	{
		#region Add to XML file

		public void SortAndAddToXml (List<Student> students, string filePath)
		{
			students.Sort ();
			AddToXml (students, filePath);
		}

		public void AddToXml (List<Student> students, string filePath)
		{
			if (!File.Exists (filePath))
				throw new FileNotFoundException ($"Missing data file: {filePath}");

			if (students == null)
				throw new ArgumentNullException ();

			if (students.Count == 0)
				throw new InvalidOperationException ("List is empty.");

			foreach (Student student in students)
			{
				AddToXml (student, filePath);
			}
		}

		public void AddToXml (Student student, string filePath)
		{
			if (!File.Exists (filePath))
				throw new FileNotFoundException ($"Missing data file: {filePath}");

			if (student == null)
				throw new ArgumentNullException ();

			XDocument xDocument = XDocument.Load (filePath);
			XElement root = xDocument.Element (StudentsXMLInfo.Students);
			IEnumerable<XElement> rows = root.Descendants (StudentsXMLInfo.Student);

			if (rows.Any ())
				rows.FirstOrDefault ()?.AddBeforeSelf (StudentToXElement (student));
			else root.Add (StudentToXElement (student));

			xDocument.Save (filePath);
		}

		#endregion

		#region Save to new XML file

		public void SortAndSaveToNewXml (StudentsCollection students, string filePath)
		{
			students.Sort ();
			SaveToNewXml (students, filePath);
		}

		public void SaveToNewXml (StudentsCollection students, string filePath)
		{
			if (students == null)
				throw new ArgumentNullException ();

			if (students.Count < 1)
				throw new ArgumentException ("Students list is empty.");

			XDocument xDocument = new XDocument ();
			XElement root = new XElement (StudentsXMLInfo.Students);
			xDocument.Add (root);

			foreach (Student student in students)
			{
				root.Add (StudentToXElement (student));
			}

			xDocument.Save (filePath);
		}

		public void SaveToNewXml (Student student, string filePath)
		{
			if (student == null)
				throw new ArgumentNullException ();

			XDocument xDocument = new XDocument ();
			XElement root = new XElement (StudentsXMLInfo.Students);
			xDocument.Add (root);
			root.Add (StudentToXElement (student));
			xDocument.Save (filePath);
		}

		#endregion

		#region Conversion Methods

		private XElement StudentToXElement (Student student)
		{
			XElement newStudent = new XElement (StudentsXMLInfo.Student,
				   new XAttribute (StudentsXMLInfo.GroupID, student.GroupID.ToString ()),
				   new XAttribute (StudentsXMLInfo.FirstName, student.FirstName),
				   new XAttribute (StudentsXMLInfo.LastName, student.LastName),
				   new XAttribute (StudentsXMLInfo.DateOfBirth, student.DateOfBirth.ToShortDateString ()),
				   new XAttribute (StudentsXMLInfo.Education, student.Education.ToString ())
			);

			newStudent.Add (ExamsToXElement (StudentsXMLInfo.Exams, student.Exams) as object);
			newStudent.Add (ExamsToXElement (StudentsXMLInfo.Tests, student.Tests) as object);

			return newStudent;
		}

		private XElement ExamsToXElement (string elementName, List<Exam> exams)
		{
			XElement newExams = new XElement (elementName);

			if (exams.Count > 0)
				foreach (Exam exam in exams)
					newExams.Add (ExamToXElement (exam, elementName.Remove (elementName.Length - 1)));

			return newExams;
		}

		private XElement ExamToXElement (Exam exam, string elementName)
		{
			return new XElement (elementName,
				new XAttribute (StudentsXMLInfo.Name, exam.Name),
				new XAttribute (StudentsXMLInfo.Mark, exam.Mark.ToString ()),
				new XAttribute (StudentsXMLInfo.DateTime, exam.DateTime.ToShortDateString ()));
		}

		#endregion
	}
}
