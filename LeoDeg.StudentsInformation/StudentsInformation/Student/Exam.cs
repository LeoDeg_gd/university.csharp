﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsInformation
{
	public class Exam
	{
		public string Name { get; set; }
		public int Mark { get; set; }
		public DateTime DateTime { get; set; }

		public Exam () : this (string.Empty, int.MinValue, DateTime.MinValue) { }

		public Exam (string name, int mark, DateTime dateTime)
		{
			Name = name;
			Mark = mark;
			DateTime = dateTime;
		}

		public override string ToString ()
		{
			return string.Format ("Subject Name: {0}, Mark: {1}, Date Time: {2}", Name, Mark, DateTime);
		}
	}
}
