﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace StudentsInformation
{
	public class StudentsObservableCollection : StudentsCollection
	{
		public EventHandler<StudentEventArgs> OnCollectionChanged;
		public EventHandler<StudentEventArgs> OnRefereceChanged;

		public override Student this[int index]
		{
			get => base[index];
			set
			{
				base[index] = value;
				OnRefereceChanged?.Invoke (this, new StudentEventArgs (NotifyCollectionChangedAction.Replace)
				{ LogInfo = "Set student value.", StudentInfo = value.ToShortString () });
				OnCollectionChanged?.Invoke (this, new StudentEventArgs (NotifyCollectionChangedAction.Add)
				{ LogInfo = "Set student value.", StudentInfo = value.ToShortString () });
			}
		}

		public override void SetRange (List<Student> students)
		{
			base.SetRange (students);
			OnRefereceChanged?.Invoke (this, new StudentEventArgs (NotifyCollectionChangedAction.Replace));
			OnCollectionChanged?.Invoke (this, new StudentEventArgs (NotifyCollectionChangedAction.Add)
			{ LogInfo = "Set list of students.", StudentInfo = "Amount of students " + students.Count });
		}

		public override void AddRange (List<Student> students)
		{
			base.AddRange (students);
			OnCollectionChanged?.Invoke (this, new StudentEventArgs (NotifyCollectionChangedAction.Add)
			{ LogInfo = "Add list of students.", StudentInfo = "Amount of students " + students.Count });
		}

		public override void Add (Student student)
		{
			base.Add (student);
			OnCollectionChanged?.Invoke (this, new StudentEventArgs (NotifyCollectionChangedAction.Add)
			{ LogInfo = "Add student.", StudentInfo = student.ToShortString () });
		}

		public override void Insert (int index, Student item)
		{
			base.Insert (index, item);
			OnCollectionChanged?.Invoke (this, new StudentEventArgs (NotifyCollectionChangedAction.Remove)
			{ LogInfo = "Insert student.", StudentInfo = item.ToShortString () });
		}

		public override bool Remove (Student student)
		{
			OnCollectionChanged?.Invoke (this, new StudentEventArgs (NotifyCollectionChangedAction.Remove)
			{ LogInfo = "Remove student.", StudentInfo = student.ToShortString () });
			return base.Remove (student);
		}

		public override void RemoveAt (int index)
		{
			OnCollectionChanged?.Invoke (this, new StudentEventArgs (NotifyCollectionChangedAction.Remove)
			{ LogInfo = "Remove student.", StudentInfo = Students[index].ToShortString () });
			base.RemoveAt (index);
		}

		public override void Clear ()
		{
			base.Clear ();
			OnCollectionChanged?.Invoke (this, new StudentEventArgs (NotifyCollectionChangedAction.Remove)
			{ LogInfo = "Clear collection.", StudentInfo = "Collection is empty." });
		}
	}
}
