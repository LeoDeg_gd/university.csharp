﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace StudentsInformation
{
	public class StudentXMLReader
	{
		public string FilePath { get; set; }

		public StudentXMLReader (string filePath)
		{
			FilePath = filePath;
		}

		/// <summary>
		/// Reload and return students information from XML file.
		/// </summary>
		/// <exception cref="FileNotFoundException" />
		/// <exception cref="InvalidOperationException" />
		public List<Student> ReloadInformationFromXMLFile ()
		{
			if (!File.Exists (FilePath))
				throw new FileNotFoundException ($"Missing data file: {FilePath} \nCreate the missing file.");

			XmlDocument document = new XmlDocument ();
			document.Load (StudentsXMLInfo.FILE_PATH);

			try
			{
				return LoadStudentsFromNodes (document.SelectNodes (StudentsXMLInfo.STUDENTS_NODE));
			}
			catch (ArgumentNullException)
			{
				throw new InvalidOperationException ("XML path '/Students/Student' is empty.");
			}
		}

		private List<Student> LoadStudentsFromNodes (XmlNodeList nodes)
		{
			if (nodes == null)
				throw new ArgumentNullException ();

			List<Student> students = new List<Student> ();

			foreach (XmlNode node in nodes)
			{
				Student student = XmlNodeToStudent (node);
				student.Exams.AddRange (LoadStudentExamsFromNodeList (node.SelectNodes ($"./{StudentsXMLInfo.Exams}/{StudentsXMLInfo.Exam}")));
				student.Tests.AddRange (LoadStudentExamsFromNodeList (node.SelectNodes ($"./{StudentsXMLInfo.Tests}/{StudentsXMLInfo.Test}")));
				students.Add (student);
			}

			return students;
		}

		private static Student XmlNodeToStudent (XmlNode node)
		{
			return new Student (node.AttributeAsInt (StudentsXMLInfo.GroupID),
				node.AttributeAsString (StudentsXMLInfo.FirstName),
				node.AttributeAsString (StudentsXMLInfo.LastName),
				node.AttributeAsDateTime (StudentsXMLInfo.DateOfBirth),
				node.AttributeAsEducation (StudentsXMLInfo.Education));
		}

		private List<Exam> LoadStudentExamsFromNodeList (XmlNodeList examNodes)
		{
			if (examNodes == null)
				throw new ArgumentNullException ($"Node {examNodes} does not exists.");

			if (examNodes.Count == 0)
				return new List<Exam> ();

			List<Exam> exams = new List<Exam> ();

			foreach (XmlNode exam in examNodes)
			{
				exams.Add (new Exam (
					exam.AttributeAsString (StudentsXMLInfo.Name),
					exam.AttributeAsInt (StudentsXMLInfo.Mark),
					exam.AttributeAsDateTime (StudentsXMLInfo.DateTime))
				);
			}

			return exams;
		}
	}
}
