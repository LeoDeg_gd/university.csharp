﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsInformation
{
	public class StudentEventArgs : EventArgs
	{
		public string LogInfo { get; set; }
		public string StudentInfo { get; set; }
		public DateTime EventTime { get; set; }
		public NotifyCollectionChangedAction Action { get; set; }

		public StudentEventArgs (NotifyCollectionChangedAction action)
		{
			Action = action;
			EventTime = DateTime.Now;
		}
	}
}
