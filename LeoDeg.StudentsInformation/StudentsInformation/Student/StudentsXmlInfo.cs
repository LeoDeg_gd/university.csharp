﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsInformation
{
	public static class StudentsXMLInfo
	{
		public const string FILE_PATH = "StudentsInformation.xml";
		public const string STUDENTS_NODE = "/Students/Student";

		public const string Students = "Students";
		public const string Student = "Student";

		public const string GroupID = "GroupID";
		public const string FirstName = "FirstName";
		public const string LastName = "LastName";
		public const string DateOfBirth = "DateOfBirth";
		public const string Education = "Education";

		public const string Exams = "Exams";
		public const string Exam = "Exam";
		public const string Tests = "Tests";
		public const string Test = "Test";

		public const string Name = "Name";
		public const string Mark = "Mark";
		public const string DateTime = "DateTime";
	}
}
