﻿namespace StudentsInformation
{
	public class StudentsManagementSystem
	{
		public Logs Logs { get; }
		public StudentsObservableCollection StudentsCollection { get; }

		private readonly StudentXMLReader xmlReader;
		private readonly StudentXMLWriter xmlWriter;

		public StudentsManagementSystem ()
		{
			Logs = new Logs ();
			xmlReader = new StudentXMLReader (StudentsXMLInfo.FILE_PATH);
			xmlWriter = new StudentXMLWriter ();
			StudentsCollection = new StudentsObservableCollection ();
			StudentsCollection.OnCollectionChanged += Logs.CollectionChanged;
		}

		public void ReloadInformationFromXMLFile ()
		{
			StudentsCollection.SetRange (xmlReader.ReloadInformationFromXMLFile ());
		}

		public void SaveInformationToXMLFile ()
		{
			xmlWriter.SaveToNewXml (StudentsCollection, StudentsXMLInfo.FILE_PATH);
		}
	}
}
