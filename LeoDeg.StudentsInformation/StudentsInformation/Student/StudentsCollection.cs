﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StudentsInformation
{
	public class StudentsCollection : IList<Student>
	{
		protected List<Student> Students { get; set; }
		public enum FindType { Exams, Tests }
		public enum SortType { Default, FirtName, LastName, DateOfBirth, AverageExamsRating }
		public int Count { get { return Students.Count; } }
		public double MaxAverageRating { get { return Students.Max ((x) => x.AverageExamsRating); } }
		public bool IsReadOnly => ((IList<Student>)Students).IsReadOnly;

		public StudentsCollection ()
		{
			Students = new List<Student> ();
		}

		public void InitializeDefaultStudentsCollection (int studentsAmount)
		{
			lock (Students)
			{
				for (int i = 0; i < studentsAmount; i++)
				{
					this.Add (new Student (i, "NoName" + i.ToString (), "NoLastName" + i.ToString (), DateTime.Now, Education.None));
				}
			}
		}

		public virtual Student this[int index]
		{
			get { return Students[index]; }
			set { lock (Students) Students[index] = value; }
		}

		public virtual void AddRange (List<Student> students)
		{
			lock (students)
				Students.AddRange (students);
		}

		public virtual void SetRange (List<Student> students)
		{
			lock (students)
				Students = students;
		}

		public virtual void Add (Student student)
		{
			lock (Students)
				Students.Add (student);
		}

		public virtual bool Remove (Student student)
		{
			lock (Students)
				return Students.Remove (student);
		}

		public int IndexOf (Student item)
		{
			lock (Students)
				return Students.IndexOf (item);
		}

		public virtual void Insert (int index, Student item)
		{
			lock (Students)
				Students.Insert (index, item);
		}

		public virtual void RemoveAt (int index)
		{
			lock (Students)
				Students.RemoveAt (index);
		}

		public virtual void Clear ()
		{
			lock (Students)
				Students.Clear ();
		}

		public bool Contains (Student item)
		{
			lock (Students)
				return Students.Contains (item);
		}

		public void CopyTo (Student[] array, int arrayIndex)
		{
			Students.CopyTo (array, arrayIndex);
		}

		public void Sort (SortType sortType = SortType.Default)
		{
			lock (Students)
			{
				switch (sortType)
				{
					case SortType.Default:
						Students.Sort ();
						break;
					case SortType.FirtName:
						Students.Sort ((x, y) => x.FirstName.CompareTo (y.FirstName));
						break;
					case SortType.LastName:
						Students.Sort ((x, y) => x.LastName.CompareTo (y.LastName));
						break;
					case SortType.DateOfBirth:
						Students.Sort ((x, y) => x.DateOfBirth.CompareTo (y.DateOfBirth));
						break;
					case SortType.AverageExamsRating:
						Students.Sort ((x, y) => x.AverageExamsRating.CompareTo (y.AverageExamsRating));
						break;
					default:
						Students.Sort ();
						break;
				}
			}
		}

		public Student FindStudent (string groupID, string firstName, string lastName, string dateOfBirth)
		{
			lock (Students)
			{
				int id = int.Parse (groupID);
				DateTime dateTime = DateTime.Parse (dateOfBirth);
				return Students.Find (x => x.GroupID == id
					&& x.FirstName == firstName
					&& x.LastName == lastName
					&& x.DateOfBirth.Date == dateTime.Date);
			}
		}

		public IEnumerable<Student> FindStudents (Education education)
		{
			lock (Students)
				return Students.Where ((x) => x.Education.Equals (education));
		}


		public IEnumerable<Student> FindStudents (double averageRating, FindType findType = FindType.Exams)
		{
			lock (Students)
			{
				switch (findType)
				{
					case FindType.Exams:
						return Students.Where ((x) => x.AverageExamsRating == averageRating);
					case FindType.Tests:
						return Students.Where ((x) => x.AverageTestsRating == averageRating);
					default:
						throw new ArgumentException ();
				}
			}
		}

		public override string ToString ()
		{
			lock (Students)
			{
				StringBuilder builder = new StringBuilder ();

				foreach (Student student in Students)
				{
					builder.Append (student.ToString ());
					builder.Append ("\n");
				}

				return builder.ToString ();
			}
		}

		public IEnumerator<Student> GetEnumerator ()
		{
			return Students.GetEnumerator ();
		}

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return Students.GetEnumerator ();
		}
	}
}
