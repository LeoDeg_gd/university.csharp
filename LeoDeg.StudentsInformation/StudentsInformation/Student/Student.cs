﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsInformation
{
	public class Student : Person, IComparer<Student>
	{
		public int GroupID { get; set; }
		public Education Education { get; set; }
		public List<Exam> Exams { get; set; }
		public List<Exam> Tests { get; set; }

		#region Properties

		/// <summary>
		/// Get average student's Exams rating.
		/// </summary>
		/// <exception cref="InvalidOperationException" />
		public double AverageExamsRating
		{
			get
			{
				if (Exams.Count < 1)
					return 0.0;
				return GetTotalExamsRating / Exams.Count;
			}
		}

		public double GetTotalExamsRating
		{
			get { return Exams.Sum (x => x.Mark); }
		}

		public double AverageTestsRating
		{
			get
			{
				if (Tests.Count < 1)
					throw new InvalidOperationException ("Tests is empty.");
				return GetTotalTestsRating / Tests.Count;
			}
		}

		public double GetTotalTestsRating
		{
			get { return Tests.Sum (x => x.Mark); }
		}

		#endregion

		#region Constructors

		public Student ()
		{
			Exams = new List<Exam> ();
			Tests = new List<Exam> ();
		}

		public Student (int groupID, Person person, Education education)
			: this (groupID, person.FirstName, person.LastName, person.DateOfBirth, education, new List<Exam> (), new List<Exam> ()) { }

		public Student (int groupID, string firstName, string lastName, DateTime dateOfBirth, Education education)
			: this (groupID, firstName, lastName, dateOfBirth, education, new List<Exam> (), new List<Exam> ()) { }

		public Student (int groupID, string firstName, string lastName, DateTime dateOfBirth, Education education, List<Exam> exams, List<Exam> tests)
		{
			GroupID = groupID;
			FirstName = firstName;
			LastName = lastName;
			DateOfBirth = dateOfBirth;
			Education = education;
			Exams = exams;
			Tests = tests;
		}

		#endregion

		#region Find

		public Exam FindExam (string subjectName)
		{
			return Exams.First (x => x.Name == subjectName);
		}

		public Exam FindExam (int mark)
		{
			return Exams.First (x => x.Mark == mark);
		}

		public Exam FindExam (DateTime dateTime)
		{
			return Exams.First (x => x.DateTime == dateTime);
		}

		public List<Exam> FindExams (string subjectName)
		{
			return Exams.FindAll (x => x.Name == subjectName);
		}

		public List<Exam> FindExams (int mark)
		{
			return Exams.FindAll (x => x.Mark == mark);
		}

		public List<Exam> FindExams (DateTime dateTime)
		{
			return Exams.FindAll (x => x.DateTime == dateTime);
		}

		public Exam FindTest (string subjectName)
		{
			return Tests.First (x => x.Name == subjectName);
		}

		public Exam FindTest (int mark)
		{
			return Tests.First (x => x.Mark == mark);
		}

		public Exam FindTest (DateTime dateTime)
		{
			return Tests.First (x => x.DateTime == dateTime);
		}

		public List<Exam> FindTests (string subjectName)
		{
			return Tests.FindAll (x => x.Name == subjectName);
		}

		public List<Exam> FindTests (int mark)
		{
			return Tests.FindAll (x => x.Mark == mark);
		}

		public List<Exam> FindTests (DateTime dateTime)
		{
			return Tests.FindAll (x => x.DateTime == dateTime);
		}

		#endregion

		#region To String Methods

		/// <summary>
		/// Return person info, group id, education, exams and tests' information.
		/// </summary>
		public override string ToString ()
		{
			StringBuilder stringBuilder = new StringBuilder ();
			stringBuilder.Append (ToShortString ());
			stringBuilder.Append ("\n");
			stringBuilder.Append ("\t").Append (ToStringExams ());
			stringBuilder.Append ("\n");
			stringBuilder.Append ("\t").Append (ToStringTests ());
			return stringBuilder.ToString ();
		}

		/// <summary>
		/// Return person info, group id and education information.
		/// </summary>
		public override string ToShortString ()
		{
			return string.Format ("GroupID: {0} \nFirst Name: {1}, Last Name: {2}, Date of Birth: {3} \nEducation: {4}", GroupID, FirstName, LastName, DateOfBirth.ToShortDateString (), Education.ToString ());
		}

		/// <summary>
		/// Return string with exams' information.
		/// </summary>
		public string ToStringExams ()
		{
			return ListToString ("Exams", Exams);
		}

		/// <summary>
		/// Return string with exams' information.
		/// </summary>
		public string ToStringTests ()
		{
			return ListToString ("Tests", Tests);
		}

		private string ListToString (string listName, List<Exam> list)
		{
			if (list.Count < 1)
				return listName + " is empty.\n";

			StringBuilder stringBuilder = new StringBuilder ();
			stringBuilder.Append (listName);

			foreach (var item in list)
				stringBuilder.Append ("\n\t").Append (item.ToString ());

			return stringBuilder.ToString ();
		}

		#endregion

		#region Override Methods

		public override object Clone ()
		{
			return new Student (GroupID, FirstName, LastName, DateOfBirth, Education);
		}

		public override bool Equals (object obj)
		{
			if (obj == null) return false;
			if (!(obj is Student other)) return false;

			return GroupID == other.GroupID &&
				FirstName == other.FirstName &&
				LastName == other.LastName &&
				DateOfBirth == other.DateOfBirth &&
				Education == other.Education;
		}

		public override int GetHashCode ()
		{
			return base.GetHashCode ();
		}

		/// <summary>
		/// Compare students by average exams rating.
		/// </summary>
		public int Compare (Student source, Student other)
		{
			double sourceAverageRating = source.AverageExamsRating;
			double otherAverageRating = other.AverageExamsRating;

			if (sourceAverageRating < otherAverageRating) return -1;
			if (sourceAverageRating > otherAverageRating) return 1;
			return 0;
		}

		#endregion
	}
}
