using NUnit.Framework;
using LeoDeg.MathExpressionSolver;

namespace LeoDeg.Tests
{
	public class ExpressionSolverTests
	{
		ExpressionSolver solver;

		[SetUp]
		public void Setup ()
		{
			solver = new ExpressionSolver ();
		}

		[Test]
		public void Solve_SolveAddExpression_ReturnRightAnswer ()
		{
			decimal result = solver.Evaluate ("5+5+5");
			decimal expected = 5 + 5 + 5;

			Assert.AreEqual (expected, result);
		}

		[Test]
		public void Solve_SolveSubstractExpression_ReturnRightAnswer ()
		{
			decimal result = solver.Evaluate ("5-5-2");
			decimal expected = 5 - 5 - 2;

			Assert.AreEqual (expected, result);
		}

		[Test]
		public void Solve_SolveDivideExpression_ReturnRightAnswer ()
		{
			decimal result = solver.Evaluate ("5/5/5");
			decimal expected = 5m /5m / 5m;

			Assert.AreEqual (expected, result);
		}

		[Test]
		public void Solve_SolveMultipliExpression_ReturnRightAnswer ()
		{
			decimal result = solver.Evaluate ("5*5*2");
			decimal expected = 5m * 5m * 2m;

			Assert.AreEqual (expected, result);
		}

		[Test]
		public void Solve_SolveAddSubMultDivideExpression_ReturnRightAnswer ()
		{
			decimal result = solver.Evaluate ("3+5/2*5-1");
			decimal expected = 3m + 5m / 2m * 5m - 1m;

			Assert.AreEqual (expected, result);
		}

		[Test]
		public void Solve_SolveAddSubMultDivideExpressionWithDecimalPlace_ReturnRightAnswer ()
		{
			decimal result = solver.Evaluate ("3+5,5/2*5,1-1");
			decimal expected = 3m + 5.5m / 2m * 5.1m - 1m;

			Assert.AreEqual (expected, result);
		}
	}
}