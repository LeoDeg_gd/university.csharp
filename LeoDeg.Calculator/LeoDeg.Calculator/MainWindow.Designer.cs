﻿namespace LeoDeg.Calculator
{
	partial class MainWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose ();
			}
			base.Dispose (disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
			this.tbField = new System.Windows.Forms.TextBox();
			this.btn7 = new System.Windows.Forms.Button();
			this.btn8 = new System.Windows.Forms.Button();
			this.btn9 = new System.Windows.Forms.Button();
			this.btn4 = new System.Windows.Forms.Button();
			this.btn5 = new System.Windows.Forms.Button();
			this.btn6 = new System.Windows.Forms.Button();
			this.btn1 = new System.Windows.Forms.Button();
			this.btn2 = new System.Windows.Forms.Button();
			this.btn3 = new System.Windows.Forms.Button();
			this.btn0 = new System.Windows.Forms.Button();
			this.btnClear = new System.Windows.Forms.Button();
			this.btnEqual = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.btnSubstract = new System.Windows.Forms.Button();
			this.btnMultipli = new System.Windows.Forms.Button();
			this.btnDivide = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// tbField
			// 
			this.tbField.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.tbField.Location = new System.Drawing.Point(24, 31);
			this.tbField.Name = "tbField";
			this.tbField.Size = new System.Drawing.Size(383, 30);
			this.tbField.TabIndex = 0;
			this.tbField.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbField_KeyPress);
			// 
			// btn7
			// 
			this.btn7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btn7.Location = new System.Drawing.Point(24, 95);
			this.btn7.Name = "btn7";
			this.btn7.Size = new System.Drawing.Size(96, 53);
			this.btn7.TabIndex = 1;
			this.btn7.Text = "7";
			this.btn7.UseVisualStyleBackColor = true;
			this.btn7.Click += new System.EventHandler(this.Btn7_Click);
			// 
			// btn8
			// 
			this.btn8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btn8.Location = new System.Drawing.Point(126, 95);
			this.btn8.Name = "btn8";
			this.btn8.Size = new System.Drawing.Size(96, 53);
			this.btn8.TabIndex = 2;
			this.btn8.Text = "8";
			this.btn8.UseVisualStyleBackColor = true;
			this.btn8.Click += new System.EventHandler(this.Btn8_Click);
			// 
			// btn9
			// 
			this.btn9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btn9.Location = new System.Drawing.Point(227, 95);
			this.btn9.Name = "btn9";
			this.btn9.Size = new System.Drawing.Size(96, 53);
			this.btn9.TabIndex = 3;
			this.btn9.Text = "9";
			this.btn9.UseVisualStyleBackColor = true;
			this.btn9.Click += new System.EventHandler(this.Btn9_Click);
			// 
			// btn4
			// 
			this.btn4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btn4.Location = new System.Drawing.Point(24, 165);
			this.btn4.Name = "btn4";
			this.btn4.Size = new System.Drawing.Size(96, 53);
			this.btn4.TabIndex = 4;
			this.btn4.Text = "4";
			this.btn4.UseVisualStyleBackColor = true;
			this.btn4.Click += new System.EventHandler(this.Btn4_Click);
			// 
			// btn5
			// 
			this.btn5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btn5.Location = new System.Drawing.Point(126, 165);
			this.btn5.Name = "btn5";
			this.btn5.Size = new System.Drawing.Size(96, 53);
			this.btn5.TabIndex = 5;
			this.btn5.Text = "5";
			this.btn5.UseVisualStyleBackColor = true;
			this.btn5.Click += new System.EventHandler(this.Btn5_Click);
			// 
			// btn6
			// 
			this.btn6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btn6.Location = new System.Drawing.Point(227, 165);
			this.btn6.Name = "btn6";
			this.btn6.Size = new System.Drawing.Size(96, 53);
			this.btn6.TabIndex = 6;
			this.btn6.Text = "6";
			this.btn6.UseVisualStyleBackColor = true;
			this.btn6.Click += new System.EventHandler(this.Btn6_Click);
			// 
			// btn1
			// 
			this.btn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btn1.Location = new System.Drawing.Point(24, 234);
			this.btn1.Name = "btn1";
			this.btn1.Size = new System.Drawing.Size(96, 53);
			this.btn1.TabIndex = 7;
			this.btn1.Text = "1";
			this.btn1.UseVisualStyleBackColor = true;
			this.btn1.Click += new System.EventHandler(this.Btn1_Click);
			// 
			// btn2
			// 
			this.btn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btn2.Location = new System.Drawing.Point(126, 234);
			this.btn2.Name = "btn2";
			this.btn2.Size = new System.Drawing.Size(96, 53);
			this.btn2.TabIndex = 8;
			this.btn2.Text = "2";
			this.btn2.UseVisualStyleBackColor = true;
			this.btn2.Click += new System.EventHandler(this.Btn2_Click);
			// 
			// btn3
			// 
			this.btn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btn3.Location = new System.Drawing.Point(227, 234);
			this.btn3.Name = "btn3";
			this.btn3.Size = new System.Drawing.Size(96, 53);
			this.btn3.TabIndex = 9;
			this.btn3.Text = "3";
			this.btn3.UseVisualStyleBackColor = true;
			this.btn3.Click += new System.EventHandler(this.Btn3_Click);
			// 
			// btn0
			// 
			this.btn0.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btn0.Location = new System.Drawing.Point(24, 303);
			this.btn0.Name = "btn0";
			this.btn0.Size = new System.Drawing.Size(96, 53);
			this.btn0.TabIndex = 10;
			this.btn0.Text = "0";
			this.btn0.UseVisualStyleBackColor = true;
			this.btn0.Click += new System.EventHandler(this.Btn0_Click);
			// 
			// btnClear
			// 
			this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnClear.Location = new System.Drawing.Point(126, 303);
			this.btnClear.Name = "btnClear";
			this.btnClear.Size = new System.Drawing.Size(96, 53);
			this.btnClear.TabIndex = 11;
			this.btnClear.Text = "Clear";
			this.btnClear.UseVisualStyleBackColor = true;
			this.btnClear.Click += new System.EventHandler(this.BtnClear_Click);
			// 
			// btnEqual
			// 
			this.btnEqual.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnEqual.Location = new System.Drawing.Point(227, 303);
			this.btnEqual.Name = "btnEqual";
			this.btnEqual.Size = new System.Drawing.Size(96, 53);
			this.btnEqual.TabIndex = 12;
			this.btnEqual.Text = "=";
			this.btnEqual.UseVisualStyleBackColor = true;
			this.btnEqual.Click += new System.EventHandler(this.BtnEqual_Click);
			// 
			// btnAdd
			// 
			this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnAdd.Location = new System.Drawing.Point(329, 95);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(78, 53);
			this.btnAdd.TabIndex = 13;
			this.btnAdd.Text = "+";
			this.btnAdd.UseVisualStyleBackColor = true;
			this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
			// 
			// btnSubstract
			// 
			this.btnSubstract.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnSubstract.Location = new System.Drawing.Point(329, 165);
			this.btnSubstract.Name = "btnSubstract";
			this.btnSubstract.Size = new System.Drawing.Size(78, 53);
			this.btnSubstract.TabIndex = 14;
			this.btnSubstract.Text = "-";
			this.btnSubstract.UseVisualStyleBackColor = true;
			this.btnSubstract.Click += new System.EventHandler(this.BtnSubstract_Click);
			// 
			// btnMultipli
			// 
			this.btnMultipli.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnMultipli.Location = new System.Drawing.Point(329, 234);
			this.btnMultipli.Name = "btnMultipli";
			this.btnMultipli.Size = new System.Drawing.Size(78, 53);
			this.btnMultipli.TabIndex = 15;
			this.btnMultipli.Text = "*";
			this.btnMultipli.UseVisualStyleBackColor = true;
			this.btnMultipli.Click += new System.EventHandler(this.BtnMultipli_Click);
			// 
			// btnDivide
			// 
			this.btnDivide.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnDivide.Location = new System.Drawing.Point(329, 303);
			this.btnDivide.Name = "btnDivide";
			this.btnDivide.Size = new System.Drawing.Size(78, 53);
			this.btnDivide.TabIndex = 16;
			this.btnDivide.Text = "/";
			this.btnDivide.UseVisualStyleBackColor = true;
			this.btnDivide.Click += new System.EventHandler(this.BtnDivide_Click);
			// 
			// MainWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(430, 389);
			this.Controls.Add(this.btnDivide);
			this.Controls.Add(this.btnMultipli);
			this.Controls.Add(this.btnSubstract);
			this.Controls.Add(this.btnAdd);
			this.Controls.Add(this.btnEqual);
			this.Controls.Add(this.btnClear);
			this.Controls.Add(this.btn0);
			this.Controls.Add(this.btn3);
			this.Controls.Add(this.btn2);
			this.Controls.Add(this.btn1);
			this.Controls.Add(this.btn6);
			this.Controls.Add(this.btn5);
			this.Controls.Add(this.btn4);
			this.Controls.Add(this.btn9);
			this.Controls.Add(this.btn8);
			this.Controls.Add(this.btn7);
			this.Controls.Add(this.tbField);
			this.Name = "MainWindow";
			this.Text = "Calculator";
			this.Load += new System.EventHandler(this.MainWindow_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox tbField;
		private System.Windows.Forms.Button btn7;
		private System.Windows.Forms.Button btn8;
		private System.Windows.Forms.Button btn9;
		private System.Windows.Forms.Button btn4;
		private System.Windows.Forms.Button btn5;
		private System.Windows.Forms.Button btn6;
		private System.Windows.Forms.Button btn1;
		private System.Windows.Forms.Button btn2;
		private System.Windows.Forms.Button btn3;
		private System.Windows.Forms.Button btn0;
		private System.Windows.Forms.Button btnClear;
		private System.Windows.Forms.Button btnEqual;
		private System.Windows.Forms.Button btnAdd;
		private System.Windows.Forms.Button btnSubstract;
		private System.Windows.Forms.Button btnMultipli;
		private System.Windows.Forms.Button btnDivide;
	}
}

