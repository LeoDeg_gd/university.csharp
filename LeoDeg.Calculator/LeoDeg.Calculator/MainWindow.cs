﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LeoDeg.MathExpressionSolver;

namespace LeoDeg.Calculator
{
	public partial class MainWindow : Form
	{
		ExpressionSolver expressionSolver;

		public MainWindow ()
		{
			InitializeComponent ();
		}

		private void MainWindow_Load (object sender, EventArgs e)
		{
			expressionSolver = new ExpressionSolver ();
		}

		#region Events

		private void Btn0_Click (object sender, EventArgs e)
		{
			AddElement ('0');
		}

		private void Btn1_Click (object sender, EventArgs e)
		{
			AddElement ('1');
		}

		private void Btn2_Click (object sender, EventArgs e)
		{
			AddElement ('2');
		}

		private void Btn3_Click (object sender, EventArgs e)
		{
			AddElement ('3');
		}

		private void Btn4_Click (object sender, EventArgs e)
		{
			AddElement ('4');
		}

		private void Btn5_Click (object sender, EventArgs e)
		{
			AddElement ('5');
		}

		private void Btn6_Click (object sender, EventArgs e)
		{
			AddElement ('6');
		}

		private void Btn7_Click (object sender, EventArgs e)
		{
			AddElement ('7');
		}

		private void Btn8_Click (object sender, EventArgs e)
		{
			AddElement ('8');
		}

		private void Btn9_Click (object sender, EventArgs e)
		{
			AddElement ('9');
		}

		private void BtnAdd_Click (object sender, EventArgs e)
		{
			AddElement ('+');
		}

		private void BtnSubstract_Click (object sender, EventArgs e)
		{
			AddElement ('-');
		}

		private void BtnMultipli_Click (object sender, EventArgs e)
		{
			AddElement ('*');
		}

		private void BtnDivide_Click (object sender, EventArgs e)
		{
			AddElement ('/');
		}

		private void BtnEqual_Click (object sender, EventArgs e)
		{
			Calculate ();
		}

		private void BtnClear_Click (object sender, EventArgs e)
		{
			Clear ();
		}

		private void TbField_KeyPress (object sender, KeyPressEventArgs e)
		{
			base.OnKeyPress (e);

			if ((Keys)e.KeyChar == Keys.Escape || (Keys)e.KeyChar == Keys.Enter)
				return;

			if (char.IsLetter (e.KeyChar))
				e.Handled = true;

			if (char.IsWhiteSpace (e.KeyChar))
				e.Handled = true;

			if (!CanAddMathOperator (e.KeyChar))
				e.Handled = true;
		}

		#endregion

		private void AddElement (char element)
		{
			if (char.IsWhiteSpace (element))
				return;

			if (CanAddMathOperator (element))
				tbField.Text += element;
		}

		private bool CanAddMathOperator (char element)
		{
			if (element == '\"' || element == '\\' || element == '(' || element == ')' || element == '\'')
			{
				return false;
			}

			if (element == '+' || element == '-' || element == '/' || element == '*')
			{
				if (tbField.Text.Length < 1)
				{
					return false;
				}

				if (tbField.Text[tbField.Text.Length - 1] == '+' || tbField.Text[tbField.Text.Length - 1] == '-' ||
					tbField.Text[tbField.Text.Length - 1] == '/' || tbField.Text[tbField.Text.Length - 1] == '*')
				{
					return false;
				}
			}

			return true;
		}

		private void Calculate ()
		{
			decimal result = expressionSolver.Evaluate (tbField.Text);
			tbField.Text = result.ToString ();
		}

		private void Clear ()
		{
			tbField.Text = string.Empty;
		}
	}
}
