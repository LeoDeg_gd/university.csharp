﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LeoDeg.MathExpressionSolver
{
	public class ExpressionSolver
	{
		private Dictionary<char, Func<Expression, Expression, Expression>> operations = new Dictionary<char, Func<Expression, Expression, Expression>>
		{
			{ '+', (current, next) => Expression.Add(current, next) },
			{ '-', (current, next) => Expression.Subtract(current, next) },
			{ '*', (current, next) => Expression.Multiply(current, next) },
			{ '/', (current, next) => Expression.Divide(current, next) }
		};

		public decimal Evaluate (string expression)
		{
			foreach (var operation in operations)
			{
				if (expression.Contains (operation.Key))
				{
					var parts = expression.Split (operation.Key);
					Expression result = Expression.Constant (Evaluate (parts[0]));

					result = parts.Skip (1).Aggregate (result,
													 (current, next) =>
													 operation.Value (current, Expression.Constant (Evaluate (next))));

					var lambda = Expression.Lambda<Func<decimal>> (result);
					var compiled = lambda.Compile ();
					return compiled.Invoke ();
				}
			}

			decimal value = 0;
			decimal.TryParse (expression, out value);
			return value;
		}
	}

}

