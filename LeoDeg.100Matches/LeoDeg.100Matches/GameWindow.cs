﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeoDeg._100Matches
{
	public partial class GameWindow : Form
	{
		enum Looser { None, Player, Enemy }
		private Looser looser;
		private Random generator;

		private const int DEFAULT_MATCHES = 100;
		private int currentMatches = DEFAULT_MATCHES;

		private int playerSteps = 0;
		private int computerSteps = 0;

		private int playerMatchesAmount = 0;
		private int computerMatchesAmount = 0;

		public GameWindow ()
		{
			InitializeComponent ();

			generator = new Random ();
			looser = Looser.None;
		}

		#region Events

		private void ToolStripBtnNewGame_Click (object sender, EventArgs e)
		{
			NewGame ();
		}

		private void BtnTakeMatches_Click (object sender, EventArgs e)
		{
			GoToNextRound ();
		}

		private void TextBoxMatchesAmount_KeyPress (object sender, KeyPressEventArgs e)
		{
			base.OnKeyPress (e);

			if ((Keys)e.KeyChar == Keys.Enter || (Keys)e.KeyChar == Keys.Escape || (Keys)e.KeyChar == Keys.Back)
				return;

			if (!char.IsDigit (e.KeyChar))
				e.Handled = true;

			if (textBoxMatchesAmount.Text.Length >= 2)
				e.Handled = true;
		}

		#endregion

		#region Main Game Methods

		private void NewGame ()
		{
			textBoxMatchesAmount.ReadOnly = false;
			btnTakeMatches.Enabled = true;
			ClearGameInfo ();

			ShowMatchesInfo ();
			ShowStepsInfo ();
		}

		private void ClearGameInfo ()
		{
			currentMatches = DEFAULT_MATCHES;
			playerSteps = 0;
			computerSteps = 0;
			playerMatchesAmount = 0;
			computerMatchesAmount = 0;
		}

		private void EndGame ()
		{
			textBoxMatchesAmount.ReadOnly = true;
			btnTakeMatches.Enabled = false;

			ShowMatchesInfo ();
			ShowStepsInfo ();
			ShowEndGameInfo ();
		}

		private void GoToNextRound ()
		{
			if (PlayerTakeMatches ())
			{
				ComputerTakeMatches ();
				ShowMatchesInfo ();
				ShowStepsInfo ();
			}
		}

		#endregion

		#region Take Matches Methods

		private bool PlayerTakeMatches ()
		{
			try
			{
				int matchesAmount = int.Parse (textBoxMatchesAmount.Text);

				if (matchesAmount > 0 && matchesAmount <= 10)
				{
					looser = Looser.Player;
					++playerSteps;
					playerMatchesAmount += matchesAmount;
					TakeMatches (matchesAmount);
					return true;
				}
				else
				{
					MessageBox.Show ("Number must be between 1-10");
				}
			}
			catch (ArgumentNullException ex)
			{
				MessageBox.Show ("Argument null exception. \nError Message: " + ex.Message);
			}
			catch (FormatException ex)
			{
				MessageBox.Show ("Format exception error. \nError Message: " + ex.Message);
			}
			catch (Exception ex)
			{
				MessageBox.Show (ex.Message);
			}

			return false;
		}

		private void ComputerTakeMatches ()
		{
			looser = Looser.Enemy;
			++computerSteps;
			int generatedNumber = generator.Next (10);
			computerMatchesAmount += generatedNumber;
			TakeMatches (generatedNumber);
		}

		private void TakeMatches (int amount)
		{
			if (currentMatches > 0)
				currentMatches -= amount;

			if (currentMatches <= 0)
			{
				currentMatches = 0;
				EndGame ();
			}
		}

		#endregion

		#region Show Information

		private void ShowMatchesInfo ()
		{
			lblMatchesAmount.Text = "Matches: " + currentMatches.ToString ();
			lblPlayerMatchesAmount.Text = "Player Matches: " + playerMatchesAmount.ToString ();
			lblComputerMatchesAmount.Text = "Computer Matches: " + computerMatchesAmount.ToString ();
		}

		private void ShowStepsInfo ()
		{
			lblPlayerSteps.Text = "Player Steps: " + playerSteps.ToString ();
			lblComputerSteps.Text = "Computer Steps: " + computerSteps.ToString ();
			lblTotalSteps.Text = "Total Steps: " + (playerSteps + computerSteps).ToString ();
		}

		private void ShowEndGameInfo ()
		{
			string textResult = "GAME END";
			textResult += (looser == Looser.Enemy) ? "\nYou're Win!" : "\nYou're Lose!";
			textResult += "\n\nGAME INFORMATION:";
			textResult += "\nPlayer steps count: " + playerSteps.ToString ();
			textResult += "\nComputer steps count: " + computerSteps.ToString ();
			textResult += "\nTotal steps count: " + (playerSteps + computerSteps).ToString ();

			MessageBox.Show (textResult);
		}

		#endregion
	}
}
