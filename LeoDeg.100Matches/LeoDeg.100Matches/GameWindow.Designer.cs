﻿namespace LeoDeg._100Matches
{
	partial class GameWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose ();
			}
			base.Dispose (disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameWindow));
			this.lblMatchesAmount = new System.Windows.Forms.Label();
			this.textBoxMatchesAmount = new System.Windows.Forms.TextBox();
			this.btnTakeMatches = new System.Windows.Forms.Button();
			this.lblPlayerSteps = new System.Windows.Forms.Label();
			this.lblComputerSteps = new System.Windows.Forms.Label();
			this.lblTotalSteps = new System.Windows.Forms.Label();
			this.lblPlayerMatchesAmount = new System.Windows.Forms.Label();
			this.lblComputerMatchesAmount = new System.Windows.Forms.Label();
			this.toolStripMainMenu = new System.Windows.Forms.ToolStrip();
			this.toolStripBtnNewGame = new System.Windows.Forms.ToolStripButton();
			this.toolStripMainMenu.SuspendLayout();
			this.SuspendLayout();
			// 
			// lblMatchesAmount
			// 
			this.lblMatchesAmount.AutoSize = true;
			this.lblMatchesAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblMatchesAmount.Location = new System.Drawing.Point(144, 24);
			this.lblMatchesAmount.Name = "lblMatchesAmount";
			this.lblMatchesAmount.Size = new System.Drawing.Size(105, 20);
			this.lblMatchesAmount.TabIndex = 0;
			this.lblMatchesAmount.Text = "Matches: 100";
			// 
			// textBoxMatchesAmount
			// 
			this.textBoxMatchesAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 57F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxMatchesAmount.Location = new System.Drawing.Point(148, 67);
			this.textBoxMatchesAmount.Name = "textBoxMatchesAmount";
			this.textBoxMatchesAmount.Size = new System.Drawing.Size(101, 94);
			this.textBoxMatchesAmount.TabIndex = 1;
			this.textBoxMatchesAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxMatchesAmount_KeyPress);
			// 
			// btnTakeMatches
			// 
			this.btnTakeMatches.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnTakeMatches.Location = new System.Drawing.Point(148, 183);
			this.btnTakeMatches.Name = "btnTakeMatches";
			this.btnTakeMatches.Size = new System.Drawing.Size(101, 41);
			this.btnTakeMatches.TabIndex = 2;
			this.btnTakeMatches.Text = "Take";
			this.btnTakeMatches.UseVisualStyleBackColor = true;
			this.btnTakeMatches.Click += new System.EventHandler(this.BtnTakeMatches_Click);
			// 
			// lblPlayerSteps
			// 
			this.lblPlayerSteps.AutoSize = true;
			this.lblPlayerSteps.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblPlayerSteps.Location = new System.Drawing.Point(144, 241);
			this.lblPlayerSteps.Name = "lblPlayerSteps";
			this.lblPlayerSteps.Size = new System.Drawing.Size(115, 20);
			this.lblPlayerSteps.TabIndex = 3;
			this.lblPlayerSteps.Text = "Player Steps: 0";
			// 
			// lblComputerSteps
			// 
			this.lblComputerSteps.AutoSize = true;
			this.lblComputerSteps.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblComputerSteps.Location = new System.Drawing.Point(144, 266);
			this.lblComputerSteps.Name = "lblComputerSteps";
			this.lblComputerSteps.Size = new System.Drawing.Size(142, 20);
			this.lblComputerSteps.TabIndex = 4;
			this.lblComputerSteps.Text = "Computer Steps: 0";
			// 
			// lblTotalSteps
			// 
			this.lblTotalSteps.AutoSize = true;
			this.lblTotalSteps.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblTotalSteps.Location = new System.Drawing.Point(144, 291);
			this.lblTotalSteps.Name = "lblTotalSteps";
			this.lblTotalSteps.Size = new System.Drawing.Size(107, 20);
			this.lblTotalSteps.TabIndex = 5;
			this.lblTotalSteps.Text = "Total Steps: 0";
			// 
			// lblPlayerMatchesAmount
			// 
			this.lblPlayerMatchesAmount.AutoSize = true;
			this.lblPlayerMatchesAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblPlayerMatchesAmount.Location = new System.Drawing.Point(144, 334);
			this.lblPlayerMatchesAmount.Name = "lblPlayerMatchesAmount";
			this.lblPlayerMatchesAmount.Size = new System.Drawing.Size(134, 20);
			this.lblPlayerMatchesAmount.TabIndex = 6;
			this.lblPlayerMatchesAmount.Text = "Player Matches: 0";
			// 
			// lblComputerMatchesAmount
			// 
			this.lblComputerMatchesAmount.AutoSize = true;
			this.lblComputerMatchesAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblComputerMatchesAmount.Location = new System.Drawing.Point(144, 359);
			this.lblComputerMatchesAmount.Name = "lblComputerMatchesAmount";
			this.lblComputerMatchesAmount.Size = new System.Drawing.Size(161, 20);
			this.lblComputerMatchesAmount.TabIndex = 7;
			this.lblComputerMatchesAmount.Text = "Computer Matches: 0";
			// 
			// toolStripMainMenu
			// 
			this.toolStripMainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripBtnNewGame});
			this.toolStripMainMenu.Location = new System.Drawing.Point(0, 0);
			this.toolStripMainMenu.Name = "toolStripMainMenu";
			this.toolStripMainMenu.Size = new System.Drawing.Size(384, 25);
			this.toolStripMainMenu.TabIndex = 8;
			this.toolStripMainMenu.Text = "New Game";
			// 
			// toolStripBtnNewGame
			// 
			this.toolStripBtnNewGame.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.toolStripBtnNewGame.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBtnNewGame.Image")));
			this.toolStripBtnNewGame.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripBtnNewGame.Name = "toolStripBtnNewGame";
			this.toolStripBtnNewGame.Size = new System.Drawing.Size(69, 22);
			this.toolStripBtnNewGame.Text = "New Game";
			this.toolStripBtnNewGame.Click += new System.EventHandler(this.ToolStripBtnNewGame_Click);
			// 
			// GameWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(384, 411);
			this.Controls.Add(this.toolStripMainMenu);
			this.Controls.Add(this.lblComputerMatchesAmount);
			this.Controls.Add(this.lblPlayerMatchesAmount);
			this.Controls.Add(this.lblTotalSteps);
			this.Controls.Add(this.lblComputerSteps);
			this.Controls.Add(this.lblPlayerSteps);
			this.Controls.Add(this.btnTakeMatches);
			this.Controls.Add(this.textBoxMatchesAmount);
			this.Controls.Add(this.lblMatchesAmount);
			this.MaximumSize = new System.Drawing.Size(400, 450);
			this.MinimumSize = new System.Drawing.Size(400, 450);
			this.Name = "GameWindow";
			this.Text = "100 Matches";
			this.toolStripMainMenu.ResumeLayout(false);
			this.toolStripMainMenu.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblMatchesAmount;
		private System.Windows.Forms.TextBox textBoxMatchesAmount;
		private System.Windows.Forms.Button btnTakeMatches;
		private System.Windows.Forms.Label lblPlayerSteps;
		private System.Windows.Forms.Label lblComputerSteps;
		private System.Windows.Forms.Label lblTotalSteps;
		private System.Windows.Forms.Label lblPlayerMatchesAmount;
		private System.Windows.Forms.Label lblComputerMatchesAmount;
		private System.Windows.Forms.ToolStrip toolStripMainMenu;
		private System.Windows.Forms.ToolStripButton toolStripBtnNewGame;
	}
}

