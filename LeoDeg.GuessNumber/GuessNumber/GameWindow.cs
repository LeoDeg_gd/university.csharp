﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GuessNumber;

namespace GuessNumber
{
	public partial class GameWindow : Form
	{
		GuessNumber game;

		public GameWindow ()
		{
			InitializeComponent ();

			game = new GuessNumber ();
			game.OnGameOver += EndGame;
			GenerateNewGameNumber ();
		}

		#region Events

		private void NewGameToolStripMenuItem_Click (object sender, EventArgs e)
		{
			GenerateNewGameNumber ();
		}

		private void Button_NewGame_Click (object sender, EventArgs e)
		{
			GenerateNewGameNumber ();
		}

		private void ButtonCheck_Click (object sender, EventArgs e)
		{
			GetUserInput ();
		}

		private void TextBoxGuessField_KeyPress (object sender, KeyPressEventArgs e)
		{
			base.OnKeyPress (e);

			if ((Keys)e.KeyChar == Keys.Back || (Keys)e.KeyChar == Keys.Enter)
				return;

			if (char.IsDigit (e.KeyChar) == false)
				e.Handled = true;

			if (textBoxGuessField.Text.Length > 2)
				e.Handled = true;
		}

		#endregion

		#region Game Methods

		private void GetUserInput ()
		{
			try
			{
				int userNumber = int.Parse (textBoxGuessField.Text);
				if (userNumber >= 0 && userNumber <= 10)
					CheckUserInput (userNumber);
				else MessageBox.Show ("Number must be between 0 and 10");
			}
			catch (ArgumentNullException ex)
			{
				MessageBox.Show (string.Format ("Enter a number to the field. \nError Message: {0}", ex.Message));
			}
			catch (FormatException ex)
			{
				MessageBox.Show (string.Format (". \nError Message: {0}", ex.Message));
			}
			catch (OverflowException ex)
			{
				MessageBox.Show (string.Format ("Error Message: {0}", ex.Message));
			}
		}

		private void CheckUserInput (int guessNumber)
		{
			int guess = game.CheckGuess (guessNumber);
			if (!game.IsGameOver ())
			{
				switch (guess)
				{
					case 0:
						ShowGameInfo ("You're win.", Color.Green);
						break;
					case -1:
						ShowGameInfo ("Computer number is bigger then your.", Color.DarkOrange);
						break;
					case 1:
						ShowGameInfo ("Computer number is less then your.", Color.DarkOrange);
						break;
					default:
						ShowGameInfo ("Enter a number", Color.Red);
						break;
				}
			}
			else
			{
				EndGame (null, EventArgs.Empty);
			}
		}

		private void GenerateNewGameNumber ()
		{
			ClearGameStatus ();
			game.GenerateNumber ();
			btnCheck.Visible = true;
			btnNewGame.Visible = false;
			textBoxGuessField.Visible = true;
			ShowGameInfo ("New random number was generated.", Color.Green);
		}

		private void EndGame (object sender, EventArgs e)
		{
			ClearGameStatus ();
			btnCheck.Visible = false;
			btnNewGame.Visible = true;
			textBoxGuessField.Visible = false;
			ShowGameInfo ("Game Over!!!", Color.Red);
		}

		private void ShowGameInfo (string statusMessage, Color statusMessageColor)
		{
			lblAttempts.Text = string.Format ("Attempts: {0}", game.GetAttempts ().ToString ());
			lblGameStatus.ForeColor = statusMessageColor;
			lblGameStatus.Text = "Status: " + statusMessage;
		}

		private void ClearGameStatus ()
		{
			lblGameStatus.Text = string.Empty;
		}

		#endregion
	}
}
