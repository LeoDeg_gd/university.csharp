﻿namespace GuessNumber
{
    partial class GameWindow
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblGameRule = new System.Windows.Forms.Label();
			this.textBoxGuessField = new System.Windows.Forms.TextBox();
			this.btnCheck = new System.Windows.Forms.Button();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.newGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.lblGameStatus = new System.Windows.Forms.Label();
			this.lblAttempts = new System.Windows.Forms.Label();
			this.btnNewGame = new System.Windows.Forms.Button();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// lblGameRule
			// 
			this.lblGameRule.AutoSize = true;
			this.lblGameRule.Location = new System.Drawing.Point(74, 34);
			this.lblGameRule.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblGameRule.Name = "lblGameRule";
			this.lblGameRule.Size = new System.Drawing.Size(143, 13);
			this.lblGameRule.TabIndex = 0;
			this.lblGameRule.Text = "Guess a number from 0 to 10";
			this.lblGameRule.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// textBoxGuessField
			// 
			this.textBoxGuessField.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxGuessField.Location = new System.Drawing.Point(106, 65);
			this.textBoxGuessField.Margin = new System.Windows.Forms.Padding(2);
			this.textBoxGuessField.Name = "textBoxGuessField";
			this.textBoxGuessField.Size = new System.Drawing.Size(66, 56);
			this.textBoxGuessField.TabIndex = 1;
			this.textBoxGuessField.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxGuessField_KeyPress);
			// 
			// btnCheck
			// 
			this.btnCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnCheck.Location = new System.Drawing.Point(77, 136);
			this.btnCheck.Margin = new System.Windows.Forms.Padding(2);
			this.btnCheck.Name = "btnCheck";
			this.btnCheck.Size = new System.Drawing.Size(124, 34);
			this.btnCheck.TabIndex = 2;
			this.btnCheck.Text = "Check";
			this.btnCheck.UseVisualStyleBackColor = true;
			this.btnCheck.Click += new System.EventHandler(this.ButtonCheck_Click);
			// 
			// menuStrip1
			// 
			this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newGameToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
			this.menuStrip1.Size = new System.Drawing.Size(286, 24);
			this.menuStrip1.TabIndex = 4;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// newGameToolStripMenuItem
			// 
			this.newGameToolStripMenuItem.Name = "newGameToolStripMenuItem";
			this.newGameToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
			this.newGameToolStripMenuItem.Text = "New Game";
			this.newGameToolStripMenuItem.Click += new System.EventHandler(this.NewGameToolStripMenuItem_Click);
			// 
			// lblGameStatus
			// 
			this.lblGameStatus.AutoSize = true;
			this.lblGameStatus.Location = new System.Drawing.Point(11, 195);
			this.lblGameStatus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblGameStatus.Name = "lblGameStatus";
			this.lblGameStatus.Size = new System.Drawing.Size(0, 13);
			this.lblGameStatus.TabIndex = 5;
			// 
			// lblAttempts
			// 
			this.lblAttempts.AutoSize = true;
			this.lblAttempts.Location = new System.Drawing.Point(11, 219);
			this.lblAttempts.Name = "lblAttempts";
			this.lblAttempts.Size = new System.Drawing.Size(0, 13);
			this.lblAttempts.TabIndex = 6;
			// 
			// btnNewGame
			// 
			this.btnNewGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnNewGame.Location = new System.Drawing.Point(77, 77);
			this.btnNewGame.Margin = new System.Windows.Forms.Padding(2);
			this.btnNewGame.Name = "btnNewGame";
			this.btnNewGame.Size = new System.Drawing.Size(124, 34);
			this.btnNewGame.TabIndex = 7;
			this.btnNewGame.Text = "New Game";
			this.btnNewGame.UseVisualStyleBackColor = true;
			this.btnNewGame.Click += new System.EventHandler(this.Button_NewGame_Click);
			// 
			// GameWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(286, 253);
			this.Controls.Add(this.btnNewGame);
			this.Controls.Add(this.lblAttempts);
			this.Controls.Add(this.lblGameStatus);
			this.Controls.Add(this.btnCheck);
			this.Controls.Add(this.textBoxGuessField);
			this.Controls.Add(this.lblGameRule);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Margin = new System.Windows.Forms.Padding(2);
			this.Name = "GameWindow";
			this.Text = "Guess Number Game";
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblGameRule;
        private System.Windows.Forms.TextBox textBoxGuessField;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem newGameToolStripMenuItem;
        private System.Windows.Forms.Label lblGameStatus;
		private System.Windows.Forms.Label lblAttempts;
		private System.Windows.Forms.Button btnNewGame;
	}
}

