﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessNumber
{
	class GuessNumber
	{
		private const int MAX_ATTEMPTS = 5;

		private int attempts = MAX_ATTEMPTS;
		private int randomNumber = int.MinValue;

		public event EventHandler OnGameOver;
		public event EventHandler OnDecreaseAttempts;


		public void GenerateNumber ()
		{
			Random randomGenerator = new Random ();
			randomNumber = randomGenerator.Next (10);
			attempts = MAX_ATTEMPTS;
		}

		public int GetAttempts ()
		{
			return attempts;
		}

		public bool IsGameOver ()
		{
			return attempts < 0;
		}

		public int CheckGuess (int number)
		{
			if (randomNumber == int.MinValue) GenerateNumber ();

			DecreaseAttempts ();

			if (number < randomNumber) return -1;
			else if (number > randomNumber) return 1;
			else if (number == randomNumber) return 0;

			return int.MinValue;
		}

		private void DecreaseAttempts ()
		{
			--attempts;

			if (OnDecreaseAttempts != null)
				OnDecreaseAttempts.Invoke (null, EventArgs.Empty);

			if (attempts < 1)
			{
				if (OnGameOver != null)
					OnGameOver.Invoke (null, EventArgs.Empty);
			}
		}
	}
}
